// --
// katla is a data flow generator
// --
// Copyright (C) 2019 Twimpiex <twimpiex@gnx.be>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License version 3 as
// published by the Free Software Foundation.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.
// --
// Be aware that at any time this program may introduce bugs, backward
// incompatibilities, undesired behaviour, backdoors, ...
// Trust your testing and quality assurance, don't trust on semantic
// versioning.
//
// Contact the copyright owner if you wish to use this program with
// another license.
// --

use std::collections::HashMap;

use crate::appender::{AppenderContent, AppenderType};
use crate::config::appender::Appender as ConfigAppender;

use super::AppenderThread;

use encoding::label::encoding_from_whatwg_label;
use encoding::{DecoderTrap, EncoderTrap};

#[derive(Debug, Clone)]
pub struct Appender {
    pub appender: ConfigAppender,
    pub appenders: HashMap<String, AppenderThread>,
}

impl AppenderType for Appender {
    fn start(&mut self) {}

    fn appenders(&mut self, appenders: HashMap<String, super::AppenderThread>) {
        self.appenders = appenders;
    }

    fn append(&mut self, content: AppenderContent, flush: bool) {
        if let ConfigAppender::Appender {
            name: _,
            appender,
            charset,
        } = self.appender.clone()
        {
            let appender_thread = self.appenders.get(&appender).unwrap();
            match content {
                AppenderContent::Binary { data } => {
                    let charset_codec = encoding_from_whatwg_label(&charset).unwrap();
                    let decoded_data = charset_codec.decode(&data, DecoderTrap::Ignore).unwrap();
                    appender_thread
                        .append(AppenderContent::String { data: decoded_data }, flush)
                        .unwrap()
                }
                AppenderContent::String { data } => {
                    let charset_codec = encoding_from_whatwg_label(&charset).unwrap();
                    let encoded_data = charset_codec.encode(&data, EncoderTrap::Ignore).unwrap();
                    appender_thread
                        .append(AppenderContent::Binary { data: encoded_data }, flush)
                        .unwrap()
                }
                AppenderContent::Array { .. } => appender_thread.append(content, flush).unwrap(),
            }
        }
    }

    fn stop(&mut self) {}
}
