// --
// katla is a data flow generator
// --
// Copyright (C) 2019 Twimpiex <twimpiex@gnx.be>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License version 3 as
// published by the Free Software Foundation.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.
// --
// Be aware that at any time this program may introduce bugs, backward
// incompatibilities, undesired behaviour, backdoors, ...
// Trust your testing and quality assurance, don't trust on semantic
// versioning.
//
// Contact the copyright owner if you wish to use this program with
// another license.
// --

use crate::appender::{AppenderContent, AppenderType};
use crate::config::appender::Appender;
use std::collections::HashMap;
use std::io::Write;

#[derive(Debug)]
pub struct File {
    pub appender: Appender,
    pub file_writers: HashMap<String, std::io::BufWriter<std::fs::File>>,
}

impl AppenderType for File {
    fn start(&mut self) {
        if let Appender::File { name, file_name } = self.appender.clone() {
            let file: std::fs::File = std::fs::File::create(&file_name).expect(
                &(format!(
                    "{}{}",
                    "Unable to create file with name ".to_owned(),
                    file_name.to_owned()
                )),
            );
            let file_writer = std::io::BufWriter::new(file);
            self.file_writers.insert(name.to_string(), file_writer);
        }
    }

    fn appenders(&mut self, _appenders: std::collections::HashMap<String, super::AppenderThread>) {}

    fn append(&mut self, content: AppenderContent, flush: bool) {
        if let Appender::File { name, file_name: _ } = self.appender.clone() {
            let file_writer = self.file_writers.get_mut(&name).unwrap();
            match content {
                AppenderContent::Binary { data } => file_writer
                    .write_all(data.as_slice())
                    .expect("Unable to write data"),
                AppenderContent::String { data } => file_writer
                    .write_all(data.as_bytes())
                    .expect("Unable to write data"),
                AppenderContent::Array {
                    leading,
                    trailing,
                    separator,
                    values,
                } => {
                    match leading {
                        Some(leading) => file_writer
                            .write_all(leading.as_bytes())
                            .expect("Unable to write data"),
                        None => (),
                    }
                    let mut first = true;
                    for value in values {
                        if !first {
                            match separator.clone() {
                                Some(separator) => file_writer
                                    .write_all(separator.as_bytes())
                                    .expect("Unable to write data"),
                                None => (),
                            }
                        } else {
                            first = false;
                        }
                        file_writer
                            .write_all(value.as_bytes())
                            .expect("Unable to write data");
                    }
                    match trailing {
                        Some(trailing) => file_writer
                            .write_all(trailing.as_bytes())
                            .expect("Unable to write data"),
                        None => (),
                    }
                }
            }
            if flush {
                file_writer.flush().unwrap();
            }
        }
    }

    fn stop(&mut self) {
        if let Appender::File { name, file_name: _ } = self.appender.clone() {
            let file_writer = self.file_writers.get_mut(&name).unwrap();
            file_writer.flush().unwrap();
        }
    }
}

#[cfg(test)]
mod tests {
    //use super::*;
}
