// --
// katla is a data flow generator
// --
// Copyright (C) 2019 Twimpiex <twimpiex@gnx.be>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License version 3 as
// published by the Free Software Foundation.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.
// --
// Be aware that at any time this program may introduce bugs, backward
// incompatibilities, undesired behaviour, backdoors, ...
// Trust your testing and quality assurance, don't trust on semantic
// versioning.
//
// Contact the copyright owner if you wish to use this program with
// another license.
// --

use crate::config::entity_provider::entity_source::EntitySource;
use crate::model::entity::Entity;
use std::error::Error;

use super::entity_source;

pub fn get_random_entity_from_source(
    source: &mut EntitySource,
    include_paths: &Vec<&str>,
) -> Result<Entity, Box<dyn Error>> {
    use rand::Rng;
    match source {
        EntitySource::Csv { .. } | EntitySource::SpreadSheet { .. } => {
            get_cached_random_entity_source(source, include_paths)
        }
        EntitySource::Rng { data_type, range } => {
            trace!("data_type = {:?}", data_type);
            trace!("range = {:?}", range);
            match range {
                Some(range) => match &data_type[..] {
                    "u8" => Ok(Entity::U8(
                        rand::thread_rng().gen_range((range.low as u8)..(range.high as u8)),
                    )),
                    "i8" => Ok(Entity::I8(
                        rand::thread_rng().gen_range((range.low as i8)..(range.high as i8)),
                    )),
                    "u16" => Ok(Entity::U16(
                        rand::thread_rng().gen_range((range.low as u16)..(range.high as u16)),
                    )),
                    "i16" => Ok(Entity::I16(
                        rand::thread_rng().gen_range((range.low as i16)..(range.high as i16)),
                    )),
                    "u32" => Ok(Entity::U32(
                        rand::thread_rng().gen_range((range.low as u32)..(range.high as u32)),
                    )),
                    "i32" => Ok(Entity::I32(
                        rand::thread_rng().gen_range((range.low as i32)..(range.high as i32)),
                    )),
                    "u64" => Ok(Entity::U64(
                        rand::thread_rng().gen_range((range.low as u64)..(range.high as u64)),
                    )),
                    "i64" => Ok(Entity::I64(
                        rand::thread_rng().gen_range((range.low as i64)..(range.high as i64)),
                    )),
                    "u128" => Ok(Entity::U128(
                        rand::thread_rng().gen_range((range.low as u128)..(range.high as u128)),
                    )),
                    "i128" => Ok(Entity::I128(
                        rand::thread_rng().gen_range((range.low as i128)..(range.high as i128)),
                    )),
                    "f64" => Ok(Entity::F64(
                        rand::thread_rng().gen_range(range.low..range.high),
                    )),
                    _ => Ok(Entity::U8(
                        rand::thread_rng().gen_range((range.low as u8)..(range.high as u8)),
                    )),
                },
                None => match &data_type[..] {
                    "u8" => Ok(Entity::U8(rand::thread_rng().gen())),
                    "i8" => Ok(Entity::I8(rand::thread_rng().gen())),
                    "u16" => Ok(Entity::U16(rand::thread_rng().gen())),
                    "i16" => Ok(Entity::I16(rand::thread_rng().gen())),
                    "u32" => Ok(Entity::U32(rand::thread_rng().gen())),
                    "i32" => Ok(Entity::I32(rand::thread_rng().gen())),
                    "u64" => Ok(Entity::U64(rand::thread_rng().gen())),
                    "i64" => Ok(Entity::I64(rand::thread_rng().gen())),
                    "u128" => Ok(Entity::U128(rand::thread_rng().gen())),
                    "i128" => Ok(Entity::I128(rand::thread_rng().gen())),
                    "f64" => Ok(Entity::F64(rand::thread_rng().gen())),
                    _ => Ok(Entity::U8(rand::thread_rng().gen())),
                },
            }
        }
    }
}

pub fn get_cached_random_entity_source(
    source: &mut EntitySource,
    include_paths: &Vec<&str>,
) -> Result<Entity, Box<dyn Error>> {
    let entity_source = entity_source::get_cached_entity_source(source, include_paths)?;
    get_random_entity(&entity_source.entities, &entity_source.accumulated_weights)
}

pub fn get_random_entity(
    entity: &Vec<Vec<String>>,
    accumulated_weights: &Vec<i64>,
) -> Result<Entity, Box<dyn Error>> {
    use rand::Rng;

    let mut rng = rand::thread_rng();

    if accumulated_weights.len() == 0 {
        // no weights
        let random_number: usize = rng.gen_range(0..entity.len());

        if entity[random_number as usize].len() > 1 {
            let mut values = Vec::new();
            for col in 0..entity[random_number as usize].len() {
                values.push(Entity::String(
                    entity[random_number as usize][col].to_string(),
                ))
            }
            Ok(Entity::Array(values))
        } else {
            Ok(Entity::String(
                entity[random_number as usize][0].to_string(),
            ))
        }
    } else {
        // with weights
        get_weighted_random_entity(entity, accumulated_weights)
    }
}

// https://stackoverflow.com/questions/1761626/weighted-random-numbers
// https://xlinux.nist.gov/dads//HTML/reservoirSampling.html
fn get_weighted_random_entity(
    entity: &Vec<Vec<String>>,
    accumulated_weights: &Vec<i64>,
) -> Result<Entity, Box<dyn Error>> {
    use rand::Rng;
    let mut rng = rand::thread_rng();

    let random_number: i64 = rng.gen_range(1..=*accumulated_weights.last().unwrap());

    let idx = accumulated_weights
        .binary_search(&random_number)
        .unwrap_or_else(|x| x);

    if entity[idx].len() > 1 {
        let mut values = Vec::new();
        for col in 0..entity[idx].len() {
            values.push(Entity::String(entity[idx][col].to_string()))
        }
        Ok(Entity::Array(values))
    } else {
        Ok(Entity::String(entity[idx][0].to_string()))
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_get_random_entity_string_no_weights_no_totals_no_sum() {
        let mut entities: Vec<Vec<String>> = Vec::new();
        entities.push(vec!["value1".to_string()]);
        entities.push(vec!["value2".to_string()]);
        entities.push(vec!["value3".to_string()]);
        let totals: Vec<i64> = Vec::new();

        let mut value1_count = 0;
        let mut value2_count = 0;
        let mut value3_count = 0;
        let total_count = 1000;
        for _ in 0..total_count {
            let entity = get_random_entity(&entities, &totals).unwrap();
            match entity {
                Entity::String(string) => match string.as_str() {
                    "value1" => {
                        value1_count += 1;
                    }
                    "value2" => {
                        value2_count += 1;
                    }
                    "value3" => {
                        value3_count += 1;
                    }
                    _ => assert!(false),
                },
                _ => assert!(false),
            }
        }

        if value1_count > 400 {
            assert_eq!(value1_count, 333);
        }
        if value2_count > 400 {
            assert_eq!(value2_count, 333);
        }
        if value3_count > 400 {
            assert_eq!(value3_count, 333);
        }
    }

    #[test]
    fn test_get_random_entity_string_weighted() {
        let mut entities: Vec<Vec<String>> = Vec::new();
        entities.push(vec!["value1".to_string()]);
        entities.push(vec!["value2".to_string()]);
        entities.push(vec!["value3".to_string()]);
        let mut totals: Vec<i64> = Vec::new();
        totals.push(0);
        totals.push(1);
        totals.push(100);

        let mut value1_count = 0;
        let mut value2_count = 0;
        let mut value3_count = 0;
        let total_count = 1000;
        for _ in 0..total_count {
            let entity = get_random_entity(&entities, &totals).unwrap();
            match entity {
                Entity::String(string) => match string.as_str() {
                    "value1" => {
                        value1_count += 1;
                    }
                    "value2" => {
                        value2_count += 1;
                    }
                    "value3" => {
                        value3_count += 1;
                    }
                    _ => assert!(false),
                },
                _ => assert!(false),
            }
        }

        if value1_count > 0 {
            assert_eq!(value1_count, 0);
        }
        if value2_count > 20 {
            assert_eq!(value2_count, 10);
        }
        if value3_count < 900 {
            assert_eq!(value3_count, 990);
        }
    }

    #[test]
    fn test_get_weighted_random_entity_string() {
        let mut entities: Vec<Vec<String>> = Vec::new();
        entities.push(vec!["value1".to_string()]);
        entities.push(vec!["value2".to_string()]);
        entities.push(vec!["value3".to_string()]);

        let value1_weight = 0;
        let value2_weight = 1;
        let value3_weight = 99;

        let mut accumulated_weights: Vec<i64> = Vec::new();
        accumulated_weights.push(value1_weight);
        accumulated_weights.push(value1_weight + value2_weight);
        accumulated_weights.push(value1_weight + value2_weight + value3_weight);

        let mut value1_count = 0;
        let mut value2_count = 0;
        let mut value3_count = 0;
        let total_count = 1000;
        for _ in 0..total_count {
            let entity =
                get_weighted_random_entity(&entities.clone(), &accumulated_weights.clone()).unwrap();
            match entity {
                Entity::String(string) => match string.as_str() {
                    "value1" => {
                        value1_count += 1;
                    }
                    "value2" => {
                        value2_count += 1;
                    }
                    "value3" => {
                        value3_count += 1;
                    }
                    _ => assert!(false),
                },
                _ => assert!(false),
            }
        }

        if value1_count * accumulated_weights.last().unwrap() / total_count > (value1_weight + 1) {
            assert_eq!(
                value1_count * accumulated_weights.last().unwrap() / total_count,
                value1_weight
            );
        }
        if value2_count * accumulated_weights.last().unwrap() / total_count > (value2_weight + 1) {
            assert_eq!(
                value2_count * accumulated_weights.last().unwrap() / total_count,
                value2_weight
            );
        }
        if value3_count * accumulated_weights.last().unwrap() / total_count < (value3_weight - 1) {
            assert_eq!(
                value3_count * accumulated_weights.last().unwrap() / total_count,
                value3_weight
            );
        }
    }
}
