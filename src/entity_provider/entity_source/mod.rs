// --
// katla is a data flow generator
// --
// Copyright (C) 2019 Twimpiex <twimpiex@gnx.be>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License version 3 as
// published by the Free Software Foundation.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.
// --
// Be aware that at any time this program may introduce bugs, backward
// incompatibilities, undesired behaviour, backdoors, ...
// Trust your testing and quality assurance, don't trust on semantic
// versioning.
//
// Contact the copyright owner if you wish to use this program with
// another license.
// --

pub mod cache;
pub mod data;

use extcsv::ReaderBuilder;

use crate::config::entity_provider::entity_source::EntitySource;
use crate::Katla;
use std::error::Error;
use std::sync::Arc;

use self::cache::Cache;
use self::data::EntitySourceData;

pub fn get_cached_entity_source(
    source: &mut EntitySource,
    include_paths: &Vec<&str>,
) -> Result<Arc<EntitySourceData>, String> {
    let cache = Cache::get();
    match source {
        EntitySource::Csv {
            location,
            entities,
            weights,
            with_headers,
            totals,
        } => {
            let mut cache_entry_name: String = "".to_owned();
            cache_entry_name.push_str(location);
            cache_entry_name.push_str(entities);
            cache_entry_name.push_str(weights);
            cache_entry_name.push_str(&format!("{}", with_headers));
            cache_entry_name.push_str(totals);
            if !cache
                .lock()
                .unwrap()
                .entity_source_cache_contains(cache_entry_name.as_str())
            {
                cache.lock().unwrap().entity_source_cache_insert(
                    cache_entry_name.to_string(),
                    get_entity_source(source, include_paths)?,
                );
            }
            Ok(cache
                .lock()
                .unwrap()
                .entity_source_cache_get(cache_entry_name.as_str())
                .unwrap())
        }

        EntitySource::SpreadSheet {
            location,
            entities: _,
            weights: _,
            with_headers: _,
            totals: _,
        } => {
            let mut cache_entry_name: String = "".to_owned();
            cache_entry_name.push_str(location);
            if !cache
                .lock()
                .unwrap()
                .entity_source_cache_contains(cache_entry_name.as_str())
            {
                cache.lock().unwrap().entity_source_cache_insert(
                    cache_entry_name.to_string(),
                    EntitySourceData {
                        entities: Vec::new(),
                        accumulated_weights: Vec::new(),
                    },
                );
            }
            Ok(cache
                .lock()
                .unwrap()
                .entity_source_cache_get(cache_entry_name.as_str())
                .unwrap())
        }

        EntitySource::Rng { .. } => Err("not implemented".to_string()),
    }
}

pub fn get_entity_source(
    source: &mut EntitySource,
    include_paths: &Vec<&str>,
) -> Result<EntitySourceData, String> {
    match source {
        EntitySource::Csv {
            location,
            entities,
            weights,
            with_headers: _,
            totals: _,
        } => {
            use crate::csv::util::*;
            let entity_ranges = convert_spread_sheet_range(&Vec::new(), entities).unwrap();

            let mut entity = Vec::new();
            let mut weight = Vec::new();
            let mut accumulated_weights = Vec::new();
            let mut sum: i64 = 0;

            // TODO strip file:// or anything else correctly
            let path = location[7..].to_string();

            let loaded_source = get_csv_source(path.to_string(), include_paths).expect(
                &(format!(
                    "{}{}{}",
                    "CSV source ".to_owned(),
                    path.to_string(),
                    " not found"
                )),
            );
            let (_name, records) = loaded_source.get(0).unwrap();

            for entity_range in entity_ranges {
                if entity_range.1 == entity_range.3 {
                    // 1 row
                    let mut last_col = records.len() - 1;
                    if entity_range.4 > -1 {
                        last_col = entity_range.4 as usize;
                    }
                    for col in entity_range.2..=last_col as i32 {
                        let mut entity_entry = Vec::new();
                        entity_entry
                            .push(records[entity_range.1 as usize][col as usize].to_string());
                        entity.push(entity_entry);
                    }
                } else if entity_range.2 == entity_range.4 {
                    // 1 column
                    let mut last_row = records.len() - 1;
                    if entity_range.3 > -1 {
                        last_row = entity_range.3 as usize;
                    }
                    for row in entity_range.1..=last_row as i32 {
                        let mut entity_entry = Vec::new();
                        entity_entry
                            .push(records[row as usize][entity_range.2 as usize].to_string());
                        entity.push(entity_entry);
                    }
                } else {
                    // multi row/column
                    let mut last_row = records.len() - 1;
                    if entity_range.3 > -1 {
                        last_row = entity_range.3 as usize;
                    }
                    for row in entity_range.1..=last_row as i32 {
                        let mut entity_entry = Vec::new();

                        let mut last_col = records[row as usize].len() - 1;
                        if entity_range.4 > -1 {
                            last_col = entity_range.4 as usize;
                        }
                        for col in entity_range.2..=last_col as i32 {
                            entity_entry.push(records[row as usize][col as usize].to_string());
                        }

                        entity.push(entity_entry);
                    }
                }
            }

            if weights != "" {
                let weight_ranges = convert_spread_sheet_range(&Vec::new(), weights).unwrap();
                for weight_range in weight_ranges {
                    if weight_range.1 == weight_range.3 {
                        // 1 row
                        let mut last_col = records.len() - 1;
                        if weight_range.4 > -1 {
                            last_col = weight_range.4 as usize;
                        }
                        for col in weight_range.2..=last_col as i32 {
                            weight.push(
                                records[weight_range.1 as usize][col as usize]
                                    .parse::<i64>()
                                    .unwrap_or_else(|_i| 0),
                            );
                            sum += weight.last().unwrap();
                            accumulated_weights.push(sum);
                        }
                    } else if weight_range.2 == weight_range.4 {
                        // 1 column
                        let mut last_row = records.len() - 1;
                        if weight_range.3 > -1 {
                            last_row = weight_range.3 as usize;
                        }
                        for row in weight_range.1..=last_row as i32 {
                            weight.push(
                                records[row as usize][weight_range.2 as usize]
                                    .parse::<i64>()
                                    .unwrap_or_else(|_i| 0),
                            );
                            sum += weight.last().unwrap();
                            accumulated_weights.push(sum);
                        }
                    } else {
                        // multi row/column
                        // unsupported
                    }
                }
            }

            Ok(EntitySourceData {
                entities: entity,
                accumulated_weights,
            })
        }

        EntitySource::SpreadSheet { .. } => Err("not implemented".to_string()),

        EntitySource::Rng { .. } => Err("not implemented".to_string()),
    }
}

fn get_csv_source(
    path: String,
    include_paths: &Vec<&str>,
) -> Result<Vec<(String, Vec<Vec<String>>)>, Box<dyn Error>> {
    let path = Katla::path_on_include_path(path, include_paths);

    let mut rows = Vec::new();

    let mut rdr = ReaderBuilder::new().has_headers(false).from_path(path)?;
    for result in rdr.records() {
        let record = result?;
        let mut row = Vec::new();
        for cell in record.iter() {
            row.push(cell.to_string());
        }
        rows.push(row);
    }

    let mut values = Vec::new();
    values.push(("unknown".to_string(), rows));
    Ok(values)
}

#[allow(dead_code)]
fn get_spread_sheet_source(
    _path: String,
) -> Result<Vec<(String, Vec<Vec<String>>)>, Box<dyn Error>> {
    Ok(Vec::new())
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_get_csv_source() {
        let include_paths: Vec<&str> = vec![concat!(
            env!("CARGO_MANIFEST_DIR"),
            "/resources",
            "/tests",
            "/config",
            "/entity_provider",
            "/source",
        )];
        let source = get_csv_source("source.csv".to_string(), &include_paths);

        match source {
            Ok(source) => {
                assert_eq!(source.len(), 1);
                assert_eq!(source.get(0).unwrap().0, "unknown");
                assert_eq!(source.get(0).unwrap().1.len(), 4);
                assert_eq!(source.get(0).unwrap().1.get(0).unwrap().len(), 2);
                assert_eq!(
                    source.get(0).unwrap().1.get(0).unwrap().get(0).unwrap(),
                    "value"
                );
                assert_eq!(
                    source.get(0).unwrap().1.get(0).unwrap().get(1).unwrap(),
                    "weight"
                );
                assert_eq!(source.get(0).unwrap().1.get(1).unwrap().len(), 2);
                assert_eq!(
                    source.get(0).unwrap().1.get(1).unwrap().get(0).unwrap(),
                    "value1"
                );
                assert_eq!(
                    source.get(0).unwrap().1.get(1).unwrap().get(1).unwrap(),
                    "1"
                );
                assert_eq!(source.get(0).unwrap().1.get(2).unwrap().len(), 2);
                assert_eq!(
                    source.get(0).unwrap().1.get(2).unwrap().get(0).unwrap(),
                    "value2"
                );
                assert_eq!(
                    source.get(0).unwrap().1.get(2).unwrap().get(1).unwrap(),
                    "2"
                );
                assert_eq!(source.get(0).unwrap().1.get(3).unwrap().len(), 2);
                assert_eq!(
                    source.get(0).unwrap().1.get(3).unwrap().get(0).unwrap(),
                    "value3"
                );
                assert_eq!(source.get(0).unwrap().1.get(3).unwrap().get(1).unwrap(), "");
            }
            _ => assert!(false),
        }
    }

    #[test]
    fn test_get_entity_source_csv() {
        let mut entity_source = EntitySource::Csv {
            location: "file://source.csv".to_string(),
            entities: "A2:A".to_string(),
            weights: "B2:B".to_string(),
            with_headers: true,
            totals: "C2:C".to_string(),
        };
        let include_paths: Vec<&str> = vec![concat!(
            env!("CARGO_MANIFEST_DIR"),
            "/resources",
            "/tests",
            "/config",
            "/entity_provider",
            "/source",
        )];
        let source = get_entity_source(&mut entity_source, &include_paths);

        match source {
            Ok(source) => {
                // entity
                assert_eq!(source.entities.len(), 3);
                assert_eq!(source.entities.get(0).unwrap().len(), 1);
                assert_eq!(source.entities.get(0).unwrap().get(0).unwrap(), "value1");
                assert_eq!(source.entities.get(1).unwrap().len(), 1);
                assert_eq!(source.entities.get(1).unwrap().get(0).unwrap(), "value2");
                assert_eq!(source.entities.get(2).unwrap().len(), 1);
                assert_eq!(source.entities.get(2).unwrap().get(0).unwrap(), "value3");

                // total
                assert_eq!(source.accumulated_weights.len(), 3);
                assert_eq!(*source.accumulated_weights.get(0).unwrap(), 1);
                assert_eq!(*source.accumulated_weights.get(1).unwrap(), 3);
                assert_eq!(*source.accumulated_weights.get(2).unwrap(), 3);
            }
            _ => assert!(false),
        }
    }
}
