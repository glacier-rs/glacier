// --
// katla is a data flow generator
// --
// Copyright (C) 2019 Twimpiex <twimpiex@gnx.be>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License version 3 as
// published by the Free Software Foundation.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.
// --
// Be aware that at any time this program may introduce bugs, backward
// incompatibilities, undesired behaviour, backdoors, ...
// Trust your testing and quality assurance, don't trust on semantic
// versioning.
//
// Contact the copyright owner if you wish to use this program with
// another license.
// --

use crate::model::context::Context;

use super::super::model::entity::Entity;

use regex::Captures;
use regex::Regex;
use std::error::Error;

static KATLA_EVAL_REGEX_STRING: &str = r"\{\{?\s*([^}\s]*)\s*\}?\}";

pub fn template_katla(context: &Context, template: String) -> Result<String, Box<dyn Error>> {
    log::info!("context = {:#?}", context);
    log::info!("template = {:#?}", template);
    lazy_static! {
        static ref KATLA_EVAL_REGEX: Regex = Regex::new(KATLA_EVAL_REGEX_STRING).unwrap();
    };

    let result = KATLA_EVAL_REGEX.replace_all(&template, |caps: &Captures| {
        expression_katla(context, caps[1].to_string())
            .unwrap()
            .to_string()
    });
    Ok(result.to_string())
}

pub fn expression_katla(context: &Context, expression: String) -> Result<Entity, Box<dyn Error>> {
    log::info!("context = {:#?}", context);
    log::info!("expression = {:#?}", expression);
    let paths: Vec<&str> = expression.split('/').collect();
    if paths.len() > 1 {
        let mut entity = context.get(&paths[0].to_string()).unwrap();
        //let mut entity = entities.get(paths[0]).unwrap_or_else(|| {error!("entity '{}' not found", paths[0]); None});
        debug!("entity = {}", entity);
        for path in &paths[1..] {
            let path_parts: Vec<&str> = path.split('[').collect();
            debug!("path parts {:#?}", path_parts);
            let path_base = path_parts[0];
            debug!("path base {:#?}", path_base);
            let path_index;
            if path_parts.len() > 1 {
                let path_index_string = &path_parts[1][0..path_parts[1].len() - 1];
                path_index = Some(path_index_string.parse::<usize>().unwrap());
                debug!("path index {:#?}", path_index);
            } else {
                path_index = None;
            }
            match entity {
                Entity::Map(value) => {
                    if value.contains_key(&path_base.to_string()) {
                        entity = value.get(&path_base.to_string()).unwrap();
                    } else {
                        error!("entity '{}' not found", path);
                    }
                }
                _ => (),
            }

            match entity {
                Entity::Array(value) => {
                    entity = &value[path_index.unwrap()];
                }
                _ => (),
            }

            debug!("entity = {}", entity);
        }
        Ok(entity.clone())
    } else {
        let path_parts: Vec<&str> = paths[0].split('[').collect();
        debug!("path parts {:#?}", path_parts);
        let path_base = path_parts[0];
        debug!("path base {:#?}", path_base);
        if path_parts.len() > 1 {
            let path_index_string = &path_parts[1][0..path_parts[1].len() - 1];
            let path_index = Some(path_index_string.parse::<usize>().unwrap());
            debug!("path index {:#?}", path_index);
            let mut entity = context.get(&path_base.to_string()).unwrap();
            match entity {
                Entity::Array(value) => {
                    entity = &value[path_index.unwrap()];
                }
                _ => (),
            }
            return Ok(entity.clone());
        }
        match context.get(&paths[0].to_string()) {
            Some(entity) => Ok(entity.clone()),
            None => Ok(Entity::String(format!("variable not found '{}'", paths[0]))),
            //None => Err(format!("variable not found '{}'", paths[0])),
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::eval::tests::create_test_context;

    #[test]
    fn test_template_katla_entity_timestamp() {
        let context = create_test_context();

        assert_eq!(
            template_katla(&context, "{{ entityTimestamp_epoch }}".to_string()).unwrap(),
            "1970-01-01T00:00:00"
        );

        assert_eq!(
            template_katla(
                &context,
                "{{ entityTimestamp_22221222T222222_UTC }}".to_string()
            )
            .unwrap(),
            "2222-12-22T22:22:22+00:00:00"
        );

        assert_eq!(
            template_katla(
                &context,
                "{{ entityTimestamp_22221222T000000_UTC }}".to_string()
            )
            .unwrap(),
            "2222-12-22T00:00:00+00:00:00"
        );

        assert_eq!(
            template_katla(
                &context,
                "{{ entityTimestamp_22221222T235959_UTC }}".to_string()
            )
            .unwrap(),
            "2222-12-22T23:59:59+00:00:00"
        );

        assert_eq!(
            template_katla(
                &context,
                "{{ entityTimestamp_minus22221222T222222_UTC }}".to_string()
            )
            .unwrap(),
            "-2222-12-22T22:22:22+00:00:00"
        );
    }

    #[test]
    fn test_expression_katla_entity_timestamp() {
        let context = create_test_context();

        assert_eq!(
            expression_katla(&context, "entityTimestamp_epoch".to_string()).unwrap(),
            Entity::Timestamp {
                seconds: 0,
                nanoseconds: None,
                timezone: None
            }
        );

        assert_eq!(
            expression_katla(&context, "entityTimestamp_22221222T222222_UTC".to_string()).unwrap(),
            Entity::Timestamp {
                seconds: 7983094942,
                nanoseconds: None,
                timezone: Some(0)
            }
        );

        assert_eq!(
            expression_katla(&context, "entityTimestamp_22221222T000000_UTC".to_string()).unwrap(),
            Entity::Timestamp {
                seconds: 7983014400,
                nanoseconds: None,
                timezone: Some(0)
            }
        );

        assert_eq!(
            expression_katla(&context, "entityTimestamp_22221222T235959_UTC".to_string()).unwrap(),
            Entity::Timestamp {
                seconds: 7983100799,
                nanoseconds: None,
                timezone: Some(0)
            }
        );

        assert_eq!(
            expression_katla(
                &context,
                "entityTimestamp_minus22221222T222222_UTC".to_string()
            )
            .unwrap(),
            Entity::Timestamp {
                seconds: -132255941858,
                nanoseconds: None,
                timezone: Some(0)
            }
        );
    }

    #[test]
    fn test_template_katla() {
        let context = create_test_context();

        assert_eq!(
            template_katla(&context, "{entityString}".to_string()).unwrap(),
            "String"
        );
        assert_eq!(
            template_katla(&context, "{ entityString }".to_string()).unwrap(),
            "String"
        );
        assert_eq!(
            template_katla(&context, "{{entityString}}".to_string()).unwrap(),
            "String"
        );
        assert_eq!(
            template_katla(&context, "{{ entityString }}".to_string()).unwrap(),
            "String"
        );

        assert_eq!(
            template_katla(&context, "{entityMap/child_map_entityString}".to_string()).unwrap(),
            "Map String"
        );
        assert_eq!(
            template_katla(&context, "{entityArray[6]}".to_string()).unwrap(),
            "Array String"
        );
        assert_eq!(
            template_katla(&context, "{ entityArray[6] }".to_string()).unwrap(),
            "Array String"
        );
        assert_eq!(
            template_katla(&context, "{{entityArray[6]}}".to_string()).unwrap(),
            "Array String"
        );
        assert_eq!(
            template_katla(&context, "{{ entityArray[6] }}".to_string()).unwrap(),
            "Array String"
        );
    }
}
