// --
// katla is a data flow generator
// --
// Copyright (C) 2019 Twimpiex <twimpiex@gnx.be>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License version 3 as
// published by the Free Software Foundation.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.
// --
// Be aware that at any time this program may introduce bugs, backward
// incompatibilities, undesired behaviour, backdoors, ...
// Trust your testing and quality assurance, don't trust on semantic
// versioning.
//
// Contact the copyright owner if you wish to use this program with
// another license.
// --

pub mod context;
pub mod entity;
pub mod katla;
pub mod rhai;
pub mod tera;

use crate::model::context::Context;

use super::config::eval::Eval;
use super::model::entity::Entity;
use std::error::Error;

#[derive(PartialEq, Debug, Clone)]
pub enum EvalError {
    NoEntityFoundInContextAtIndex { index: usize, context: Context },
    NoEntityFoundAtIndex { index: usize, entity: Entity },
    NoEntityFoundInContextWithKey { key: String, context: Context },
    NoEntityFoundWithKey { key: String, entity: Entity },
    NotImplemented,
}

impl std::fmt::Display for EvalError {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        match self {
            EvalError::NoEntityFoundInContextAtIndex { index, context } => {
                write!(
                    f,
                    "entity with index {} should be in context {:?}",
                    index, context
                )
            }
            EvalError::NoEntityFoundAtIndex { index, entity } => {
                write!(
                    f,
                    "entity with index {} should be in entity {:?}",
                    index, entity
                )
            }
            EvalError::NoEntityFoundInContextWithKey { key, context } => {
                write!(
                    f,
                    "entity with key {} should be in context {:?}",
                    key, context
                )
            }
            EvalError::NoEntityFoundWithKey { key, entity } => {
                write!(
                    f,
                    "entity with key {} should be in entity {:?}",
                    key, entity
                )
            }
            EvalError::NotImplemented => {
                write!(f, "not implemented")
            }
        }
    }
}

impl Error for EvalError {}

pub fn template(context: &mut Context, eval: Eval) -> Result<String, Box<dyn Error>> {
    // TODO return error
    let eval_resource = eval
        .get_resource()
        .expect(&(format!("{}{:?}", "Error while getting resource ".to_owned(), eval)))
        .get_string()
        .expect(&(format!("{}{:?}", "Error while getting resource ".to_owned(), eval)));
    match eval {
        Eval::Katla { .. } => katla::template_katla(context, eval_resource),
        Eval::Rhai { .. } => rhai::template_rhai(context, eval_resource),
        Eval::Tera { .. } => tera::template_tera(context, eval_resource),
    }
}

pub fn expression(context: &Context, eval: &mut Eval) -> Result<Entity, Box<dyn Error>> {
    // TODO return error
    let eval_resource = eval
        .get_resource()
        .expect(&(format!("{}{:?}", "Error while getting resource ".to_owned(), eval)))
        .get_string()
        .expect(&(format!("{}{:?}", "Error while getting resource ".to_owned(), eval)));
    match eval {
        Eval::Katla { .. } => katla::expression_katla(context, eval_resource),
        Eval::Rhai { .. } => rhai::expression_rhai(context, eval_resource),
        Eval::Tera { .. } => tera::expression_tera(context, eval_resource),
    }
}

fn format_f64(value: f64, format: String) -> Result<String, String> {
    use format_num::NumberFormat;
    let number_format = NumberFormat::new();
    Ok(number_format.format(&format, value))
}

fn _format_f32(value: f32, format: String) -> Result<String, String> {
    use format_num::NumberFormat;
    let number_format = NumberFormat::new();
    Ok(number_format.format(&format, value))
}

fn _format_i128(value: i128, format: String) -> Result<String, String> {
    use format_num::NumberFormat;
    let number_format = NumberFormat::new();
    Ok(number_format.format(&format, value as f64))
}

fn _format_u128(value: u128, format: String) -> Result<String, String> {
    use format_num::NumberFormat;
    let number_format = NumberFormat::new();
    Ok(number_format.format(&format, value as f64))
}

fn format_i64(value: i64, format: String) -> Result<String, String> {
    use format_num::NumberFormat;
    let number_format = NumberFormat::new();
    Ok(number_format.format(&format, value as f64))
}

fn format_u64(value: u64, format: String) -> Result<String, String> {
    use format_num::NumberFormat;
    let number_format = NumberFormat::new();
    Ok(number_format.format(&format, value as f64))
}

fn _format_i32(value: i32, format: String) -> Result<String, String> {
    use format_num::NumberFormat;
    let number_format = NumberFormat::new();
    Ok(number_format.format(&format, value))
}

fn _format_u32(value: u32, format: String) -> Result<String, String> {
    use format_num::NumberFormat;
    let number_format = NumberFormat::new();
    Ok(number_format.format(&format, value))
}

fn _format_i16(value: i16, format: String) -> Result<String, String> {
    use format_num::NumberFormat;
    let number_format = NumberFormat::new();
    Ok(number_format.format(&format, value))
}

fn _format_u16(value: u16, format: String) -> Result<String, String> {
    use format_num::NumberFormat;
    let number_format = NumberFormat::new();
    Ok(number_format.format(&format, value))
}

fn _format_i8(value: i8, format: String) -> Result<String, String> {
    use format_num::NumberFormat;
    let number_format = NumberFormat::new();
    Ok(number_format.format(&format, value))
}

fn _format_u8(value: u8, format: String) -> Result<String, String> {
    use format_num::NumberFormat;
    let number_format = NumberFormat::new();
    Ok(number_format.format(&format, value))
}

// https://docs.rs/chrono/0.4.0/chrono/format/strftime/index.html
use chrono::DateTime;
//use chrono::NaiveDateTime;
use chrono::FixedOffset;
use chrono::Utc;
fn _format_date_time(value: DateTime<FixedOffset>, format: String) -> Result<String, String> {
    Ok(value.format(&format).to_string())
}

fn format_date_time_utc(value: DateTime<Utc>, format: String) -> Result<String, String> {
    Ok(value.format(&format).to_string())
}

#[cfg(test)]
mod tests {
    use super::*;
    use indexmap::IndexMap;

    pub fn create_test_context() -> Context {
        let mut child_map_entities = IndexMap::new();
        child_map_entities.insert("child_map_entityI8".to_string(), Entity::I8(-8));
        child_map_entities.insert("child_map_entityU8".to_string(), Entity::U8(8));
        child_map_entities.insert("child_map_entityI64".to_string(), Entity::I64(-64));
        child_map_entities.insert("child_map_entityU64".to_string(), Entity::U64(64));
        child_map_entities.insert("child_map_entityI128".to_string(), Entity::I128(-128));
        child_map_entities.insert("child_map_entityU128".to_string(), Entity::U128(128));
        child_map_entities.insert(
            "child_map_entityString".to_string(),
            Entity::String("Map String".to_string()),
        );

        let mut child_array_entities = Vec::new();
        child_array_entities.push(Entity::I8(-8));
        child_array_entities.push(Entity::U8(8));
        child_array_entities.push(Entity::I64(-64));
        child_array_entities.push(Entity::U64(64));
        child_array_entities.push(Entity::I128(-128));
        child_array_entities.push(Entity::U128(128));
        child_array_entities.push(Entity::String("Array String".to_string()));

        let mut context = Context::local();
        context.insert(Some("entityI8".to_string()), Entity::I8(-8));
        context.insert(Some("entityU8".to_string()), Entity::U8(8));
        context.insert(Some("entityI64".to_string()), Entity::I64(-64));
        context.insert(Some("entityU64".to_string()), Entity::U64(64));
        context.insert(Some("entityI128".to_string()), Entity::I128(-128));
        context.insert(Some("entityU128".to_string()), Entity::U128(128));
        context.insert(
            Some("entityTimestamp_epoch".to_string()),
            Entity::Timestamp {
                seconds: 0,
                nanoseconds: None,
                timezone: None,
            },
        );
        context.insert(
            Some("entityTimestamp_22221222T222222_UTC".to_string()),
            Entity::Timestamp {
                seconds: 7983094942,
                nanoseconds: None,
                timezone: Some(0),
            },
        );
        context.insert(
            Some("entityTimestamp_22221222T000000_UTC".to_string()),
            Entity::Timestamp {
                seconds: 7983014400,
                nanoseconds: None,
                timezone: Some(0),
            },
        );
        context.insert(
            Some("entityTimestamp_22221222T235959_UTC".to_string()),
            Entity::Timestamp {
                seconds: 7983100799,
                nanoseconds: None,
                timezone: Some(0),
            },
        );
        context.insert(
            Some("entityTimestamp_minus22221222T222222_UTC".to_string()),
            Entity::Timestamp {
                seconds: -132255941858,
                nanoseconds: None,
                timezone: Some(0),
            },
        );
        context.insert(
            Some("entityString".to_string()),
            Entity::String("String".to_string()),
        );
        context.insert(
            Some("entityMap".to_string()),
            Entity::Map(child_map_entities),
        );
        context.insert(
            Some("entityArray".to_string()),
            Entity::Array(child_array_entities),
        );

        context
    }
}
