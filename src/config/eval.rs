// --
// katla is a data flow generator
// --
// Copyright (C) 2019 Twimpiex <twimpiex@gnx.be>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License version 3 as
// published by the Free Software Foundation.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.
// --
// Be aware that at any time this program may introduce bugs, backward
// incompatibilities, undesired behaviour, backdoors, ...
// Trust your testing and quality assurance, don't trust on semantic
// versioning.
//
// Contact the copyright owner if you wish to use this program with
// another license.
// --

// katla::config::eval

use serde::{Deserialize, Serialize};
//use std::fmt::Display;
use std::str::FromStr;

use crate::config::resource::Resource;
use crate::serde::error::SerDeError;
use crate::serde::util::string_or_map;

// for template benchmark see https://github.com/djc/template-benchmarks-rs
#[derive(Deserialize, Serialize, Debug, Clone, Eq, PartialEq)]
#[serde(tag = "type", rename_all = "camelCase")]
pub enum Eval {
    Katla {
        #[serde(deserialize_with = "string_or_map")]
        uri: Resource,
    },
    Rhai {
        #[serde(deserialize_with = "string_or_map")]
        uri: Resource,
    },
    Tera {
        #[serde(deserialize_with = "string_or_map")]
        uri: Resource,
    },
}

impl FromStr for Eval {
    type Err = SerDeError;

    fn from_str(string: &str) -> Result<Self, Self::Err> {
        if let Ok(resource) = Resource::from_str(string) {
            match resource.media_type() {
                Some("application/x.katla-rs.rhai") => Ok(Eval::Rhai { uri: resource }),
                Some("application/x.katla-rs.tera") => Ok(Eval::Tera { uri: resource }),
                // This case is only if no data:application/x.katla-rs.xxx header is present
                // so we will take the entire string as Katla Eval
                _ => Ok(Eval::Katla { uri: resource }),
            }
        } else {
            Ok(Eval::Katla {
                uri: Resource::Data {
                    media_type: "text/plain;charset=US-ASCII".to_string(),
                    data: string.to_string(),
                },
            })
        }
    }
}

impl Eval {
    pub fn get_resource(&self) -> Result<&Resource, String> {
        match self {
            Eval::Katla { uri } => Ok(uri),
            Eval::Rhai { uri } => Ok(uri),
            Eval::Tera { uri } => Ok(uri),
        }
    }
}

/*
impl Display for Eval {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        match self {
            Eval::Katla { inline, file: _ } => {
                write!(f, "{}", inline.clone().unwrap())
            },
            Eval::Rhai { inline, file: _ } => {
                write!(f, "{}", inline.clone().unwrap())
            },
            Eval::Tera { inline, file: _ } => {
                write!(f, "{}", inline.clone().unwrap())
            },
        }
    }
}
*/

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_eval_from_str() {
        assert_eq!(
            Eval::from_str("test script 1").unwrap(),
            Eval::Katla {
                uri: Resource::Data {
                    media_type: "text/plain;charset=US-ASCII".to_string(),
                    data: "test script 1".to_string(),
                }
            },
        );
        assert_eq!(
            Eval::from_str("data:application/x.katla-rs.rhai,test script 2").unwrap(),
            Eval::Rhai {
                uri: Resource::Data {
                    media_type: "application/x.katla-rs.rhai".to_string(),
                    data: "test script 2".to_string(),
                }
            },
        );
        assert_eq!(
            Eval::from_str("data:application/x.katla-rs.tera,test script 3").unwrap(),
            Eval::Tera {
                uri: Resource::Data {
                    media_type: "application/x.katla-rs.tera".to_string(),
                    data: "test script 3".to_string(),
                }
            },
        );
        //assert_eq!(Eval::from_str("urn:katla-rs:rhai:file:///path/to/file").unwrap(), Eval::Thai { inline: "test script 4".to_string(), file: None } );
        //assert_eq!(Eval::from_str("data:message/external-body;access-type=URL;URL=\"file:///path/to/directory\",Content-Type:%20application/x.katla-rs.rhai%0AContent-Transfer-Encoding:%20binary%0AContent-ID:%20<file://path/to/directory>").unwrap(), Eval::Rhai { inline: "test script 5".to_string(), file: None } );
    }

    #[test]
    fn test_eval_serialize() {
        assert_eq!(
            serde_json::to_string(&Eval::Tera {
                uri: Resource::Data {
                    media_type: "".to_string(),
                    data: "eval".to_string()
                }
            })
            .unwrap(),
            "{\"type\":\"tera\",\"uri\":{\"scheme\":\"data\",\"media_type\":\"\",\"data\":\"eval\"}}"
        );
        assert_eq!(
            serde_json::to_string(&Eval::Tera {
                uri: Resource::File {
                    media_type: None,
                    authority: None,
                    path: "file_name".to_string()
                }
            })
            .unwrap(),
            "{\"type\":\"tera\",\"uri\":{\"scheme\":\"file\",\"media_type\":null,\"authority\":null,\"path\":\"file_name\"}}"
        );
    }

    #[test]
    fn test_eval_deserialize_json_tera_data() {
        let json = "{\"type\":\"tera\",\"uri\":\"data:,eval tera data\"}";
        let eval: Eval = serde_json::from_str(&json)
            .expect(format!("error while parsing '{}' to 'eval' configuration", json).as_str());
        assert!(matches!(eval, Eval::Tera { .. }));
        match eval {
            Eval::Tera { uri } => {
                assert_eq!(
                    uri,
                    Resource::Data {
                        media_type: "text/plain;charset=US-ASCII".to_string(),
                        data: "eval tera data".to_string(),
                    }
                );
            }
            _ => assert!(false, "Wrong eval type"),
        }
    }

    #[test]
    fn test_eval_deserialize_json_tera_file() {
        let json = "{\"type\":\"tera\",\"uri\":\"file://eval_tera_file\"}";
        let eval: Eval = serde_json::from_str(&json)
            .expect(format!("error while parsing '{}' to 'eval' configuration", json).as_str());
        assert!(matches!(eval, Eval::Tera { .. }));
        match eval {
            Eval::Tera { uri } => {
                assert_eq!(
                    uri,
                    Resource::File {
                        media_type: None,
                        authority: Some("".to_string()),
                        path: "eval_tera_file".to_string(),
                    }
                );
            }
            _ => assert!(false, "Wrong eval type"),
        }
    }

    #[test]
    fn test_eval_deserialize_ron_tera_data() {
        let ron = "#![enable(implicit_some)](type:\"tera\",uri:\"data:,eval tera data\",)";
        let eval: Eval = ron::from_str(&ron)
            .expect(format!("error while parsing '{}' to 'eval' configuration", ron).as_str());
        assert!(matches!(eval, Eval::Tera { .. }));
        match eval {
            Eval::Tera { uri } => {
                assert_eq!(
                    uri,
                    Resource::Data {
                        media_type: "text/plain;charset=US-ASCII".to_string(),
                        data: "eval tera data".to_string(),
                    }
                );
            }
            _ => assert!(false, "Wrong eval type"),
        }
    }

    #[test]
    fn test_eval_deserialize_ron_tera_file() {
        let ron = "#![enable(implicit_some)](type:\"tera\",uri:\"file://eval_tera_file\",)";
        let eval: Eval = ron::from_str(&ron)
            .expect(format!("error while parsing '{}' to 'eval' configuration", ron).as_str());
        assert!(matches!(eval, Eval::Tera { .. }));
        match eval {
            Eval::Tera { uri } => {
                assert_eq!(
                    uri,
                    Resource::File {
                        media_type: None,
                        authority: Some("".to_string()),
                        path: "eval_tera_file".to_string(),
                    }
                );
            }
            _ => assert!(false, "Wrong eval type"),
        }
    }

    #[test]
    fn test_eval_deserialize_json_rhai_data() {
        let json = "{\"type\":\"rhai\",\"uri\":\"data:,eval rhai data\"}";
        let eval: Eval = serde_json::from_str(&json)
            .expect(format!("error while parsing '{}' to 'eval' configuration", json).as_str());
        assert!(matches!(eval, Eval::Rhai { .. }));
        match eval {
            Eval::Rhai { uri } => {
                assert_eq!(
                    uri,
                    Resource::Data {
                        media_type: "text/plain;charset=US-ASCII".to_string(),
                        data: "eval rhai data".to_string(),
                    }
                );
            }
            _ => assert!(false, "Wrong eval type"),
        }
    }

    #[test]
    fn test_eval_deserialize_json_rhai_file() {
        let json = "{\"type\":\"rhai\",\"uri\":\"file://eval_rhai_file\"}";
        let eval: Eval = serde_json::from_str(&json)
            .expect(format!("error while parsing '{}' to 'eval' configuration", json).as_str());
        assert!(matches!(eval, Eval::Rhai { .. }));
        match eval {
            Eval::Rhai { uri } => {
                assert_eq!(
                    uri,
                    Resource::File {
                        media_type: None,
                        authority: Some("".to_string()),
                        path: "eval_rhai_file".to_string(),
                    }
                );
            }
            _ => assert!(false, "Wrong eval type"),
        }
    }

    #[test]
    fn test_eval_deserialize_ron_rhai_data() {
        let ron = "#![enable(implicit_some)](type:\"rhai\",uri:\"data:,eval rhai data\",)";
        let eval: Eval = ron::from_str(&ron)
            .expect(format!("error while parsing '{}' to 'eval' configuration", ron).as_str());
        assert!(matches!(eval, Eval::Rhai { .. }));
        match eval {
            Eval::Rhai { uri } => {
                assert_eq!(
                    uri,
                    Resource::Data {
                        media_type: "text/plain;charset=US-ASCII".to_string(),
                        data: "eval rhai data".to_string(),
                    }
                );
            }
            _ => assert!(false, "Wrong eval type"),
        }
    }

    #[test]
    fn test_eval_deserialize_ron_rhai_file() {
        let ron = "#![enable(implicit_some)](type:\"rhai\",uri:\"file://eval_rhai_file\",)";
        let eval: Eval = ron::from_str(&ron)
            .expect(format!("error while parsing '{}' to 'eval' configuration", ron).as_str());
        assert!(matches!(eval, Eval::Rhai { .. }));
        match eval {
            Eval::Rhai { uri } => {
                assert_eq!(
                    uri,
                    Resource::File {
                        media_type: None,
                        authority: Some("".to_string()),
                        path: "eval_rhai_file".to_string(),
                    }
                );
            }
            _ => assert!(false, "Wrong eval type"),
        }
    }

    //    #[test]
    //    fn test_eval_get_resource() {
    //        let mut base_dir = PathBuf::from(env!("CARGO_MANIFEST_DIR"));
    //        base_dir.push("resources/tests/config/eval/");
    //
    //        assert_eq!(
    //            Eval::Tera {
    //                inline: None,
    //                file: None
    //            }
    //            .get_resource(),
    //            Err("Neither 'inline' nor 'file' is set".to_string())
    //        );
    //        assert_eq!(
    //            Eval::Tera {
    //                inline: Some("eval".to_string()),
    //                file: None
    //            }
    //            .get_resource(),
    //            Ok("eval".to_string())
    //        );
    //        assert_eq!(
    //            Eval::Tera {
    //                inline: None,
    //                file: Some(format!(
    //                    "{}{}",
    //                    base_dir.to_str().unwrap().to_owned(),
    //                    "get_eval_test.txt".to_string()
    //                ))
    //            }
    //            .get_resource(),
    //            Ok("get_eval_file_name\n".to_string())
    //        );
    //        assert_eq!(
    //            Eval::Tera {
    //                inline: Some("eval".to_string()),
    //                file: Some("file_name".to_string())
    //            }
    //            .get_resource(),
    //            Err("Both 'inline' and 'file' are set".to_string())
    //        );
    //    }
}
