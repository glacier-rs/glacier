// --
// katla is a data flow generator
// --
// Copyright (C) 2019 Twimpiex <twimpiex@gnx.be>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License version 3 as
// published by the Free Software Foundation.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.
// --
// Be aware that at any time this program may introduce bugs, backward
// incompatibilities, undesired behaviour, backdoors, ...
// Trust your testing and quality assurance, don't trust on semantic
// versioning.
//
// Contact the copyright owner if you wish to use this program with
// another license.
// --

use super::Identifier;
use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize, Debug, Clone)]
#[serde(tag = "type", rename_all = "camelCase")]
pub enum Appender {
    Appender {
        name: String,
        appender: String,
        charset: String,
    },
    Csv {
        name: String,
        #[serde(rename = "fileName")]
        file_name: String,
    },
    Directory {
        name: String,
        #[serde(rename = "directoryName")]
        directory_name: String,
        #[serde(rename = "fileName")]
        file_name: String,
    },
    #[cfg(feature = "elastic-elasticsearch")]
    Elasticsearch {
        name: String,
        connection: ElasticsearchConnection,
        credentials: Option<ElasticsearchCredentials>,
        threads: Option<usize>,
        index: String,
    },
    File {
        name: String,
        #[serde(rename = "fileName")]
        file_name: String,
    },
    #[cfg(feature = "kafka")]
    Kafka {
        name: String,
        // TODO this host port combination (= broker) should be multivalued
        host: String,
        port: u16,
        topic: String,
    },
    #[cfg(feature = "ibm-mq")]
    Mq {
        name: String,
        #[serde(rename = "connectionString")]
        connection_string: String, // "localhost(1414),localhost(1416)"
        #[serde(rename = "queueManager")]
        queue_manager: String, // "QM1"
        channel: Option<String>,   // "DEV.APP.SVRCONN"
        #[serde(rename = "ccdtUrl")]
        ccdt_url: Option<String>, // either ccdt_url or channel is set
        queue: Option<String>,     // = "DEV.QUEUE.1"
        topic: Option<String>,
        user: Option<String>,
        // TODO encrypt
        password: Option<String>,
        #[serde(rename = "applicationName")]
        application_name: String,
        #[serde(rename = "chipherSuite")]
        cipher_suite: Option<String>,
        bindings: Option<bool>, // true or false, defaulting to false
    },
    Void {
        name: String,
    },
}

#[cfg(feature = "elastic-elasticsearch")]
#[derive(Serialize, Deserialize, Debug, Clone)]
#[serde(tag = "type", rename_all = "camelCase")]
pub enum ElasticsearchConnection {
    SingleNode { url: String },
    Cloud { id: String },
}

#[cfg(feature = "elastic-elasticsearch")]
#[derive(Serialize, Deserialize, Debug, Clone)]
#[serde(tag = "type", rename_all = "camelCase")]
pub enum ElasticsearchCredentials {
    Basic {
        username: String,
        // TODO encrypt
        password: String,
    },
    Bearer {
        #[serde(rename = "accessToken")]
        access_token: String,
    },
    // This requires the `native-tls` or `rustls-tls` feature to be enabled.
    #[cfg(any(feature = "native-tls", feature = "rustls-tls"))]
    Certificate {
        #[serde(rename = "clientCertificate")]
        client_certificate: ClientCertificate,
    },
    ApiKey {
        id: String,
        #[serde(rename = "apiKey")]
        api_key: String,
    },
}

impl Identifier for Appender {
    type Value = String;

    fn get_identifier(&self) -> String {
        match self {
            Appender::Appender { name, .. } => name.to_string(),
            Appender::Csv { name, .. } => name.to_string(),
            Appender::Directory { name, .. } => name.to_string(),
            #[cfg(feature = "elastic-elasticsearch")]
            Appender::Elasticsearch { name, .. } => name.to_string(),
            Appender::File { name, .. } => name.to_string(),
            #[cfg(feature = "kafka")]
            Appender::Kafka { name, .. } => name.to_string(),
            #[cfg(feature = "ibm-mq")]
            Appender::Mq { name, .. } => name.to_string(),
            Appender::Void { name, .. } => name.to_string(),
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_appender_deserialize_elasticsearch_single_node() {
        // JSON
        let appender: Appender =
            serde_json::from_str(&"{\"type\":\"elasticsearch\",\"name\":\"name-test\",\"connection\":{\"type\":\"singleNode\",\"url\":\"http://localhost:9200/\"},\"index\":\"test-index\"}")
                .unwrap();
        match appender {
            Appender::Elasticsearch {
                name,
                connection: _,
                credentials: _,
                threads: _,
                index: _,
            } => {
                assert_eq!(name, "name-test");
            }
            _ => assert!(false, "Wrong appender type"),
        }

        // RON
        let appender: Appender = ron::from_str(
            &"#![enable(implicit_some)](type:\"elasticsearch\",name:\"name-test\",connection:(type:\"singleNode\",url:\"http://localhost:9200/\",),index:\"test-index\",)",
        )
        .unwrap();
        match appender {
            Appender::Elasticsearch {
                name,
                connection: _,
                credentials: _,
                threads: _,
                index: _,
            } => {
                assert_eq!(name, "name-test");
            }
            _ => assert!(false, "Wrong appender type"),
        }
    }

    #[test]
    fn test_appender_deserialize_elasticsearch_cloud() {
        // JSON
        let appender: Appender = serde_json::from_str(
            &"{\"type\":\"elasticsearch\",\"name\":\"name-test\",\"connection\":{\"type\":\"cloud\",\"id\":\"cluster_name:Y2xvdWQtZW5kcG9pbnQuZXhhbXBsZSQzZGFkZjgyM2YwNTM4ODQ5N2VhNjg0MjM2ZDkxOGExYQ==\"},\"index\":\"test-index\"}",
        )
        .unwrap();
        match appender {
            Appender::Elasticsearch {
                name,
                connection: _,
                credentials: _,
                threads: _,
                index: _,
            } => {
                assert_eq!(name, "name-test");
            }
            _ => assert!(false, "Wrong appender type"),
        }

        // RON
        let appender: Appender = ron::from_str(&"#![enable(implicit_some)](type:\"elasticsearch\",name:\"name-test\",connection:(type:\"cloud\",id:\"cluster_name:Y2xvdWQtZW5kcG9pbnQuZXhhbXBsZSQzZGFkZjgyM2YwNTM4ODQ5N2VhNjg0MjM2ZDkxOGExYQ==\",),index:\"test-index\",)").unwrap();
        match appender {
            Appender::Elasticsearch {
                name,
                connection: _,
                credentials: _,
                threads: _,
                index: _,
            } => {
                assert_eq!(name, "name-test");
            }
            _ => assert!(false, "Wrong appender type"),
        }
    }

    #[cfg(feature="java")]
    #[test]
    fn test_appender_deserialize_mq() {
        // JSON
        let appender: Appender =
            serde_json::from_str(&"{\"type\":\"mq\",\"name\":\"mq-name-test\",\"connectionString\":\"10.10.10.10(1414)\",\"queueManager\":\"TEST-QUEUE-MANAGER\",\"queue\":\"TEST-QUEUE\",\"channel\":\"TEST-CHANNEL\",\"user\":\"Test User\",\"password\":\"passw0rd\",\"applicationName\":\"App Name\"}")
                .unwrap();
        match appender {
            Appender::Mq {
                name,
                connection_string,
                queue_manager,
                channel,
                ccdt_url,
                queue,
                topic,
                user,
                password,
                application_name,
                cipher_suite,
                bindings,
            } => {
                assert_eq!(name, "mq-name-test");
            }
            _ => assert!(false, "Wrong appender type"),
        }

        // RON
        let appender: Appender = ron::from_str(
            &"#![enable(implicit_some)](type:\"mq\",name:\"mq-name-test\",connectionString:\"10.10.10.10(1414)\",queueManager:\"TEST-QUEUE-MANAGER\",queque:\"TEST-QUEUE\",channel:\"TEST-CHANNEL\",user:\"Test User\",password:\"passw0rd\",applicationName:\"App Name\",)",
        )
        .unwrap();
        match appender {
            Appender::Mq {
                name,
                connection_string,
                queue_manager,
                channel,
                ccdt_url,
                queue,
                topic,
                user,
                password,
                application_name,
                cipher_suite,
                bindings,
            } => {
                assert_eq!(name, "mq-name-test");
            }
            _ => assert!(false, "Wrong appender type"),
        }
    }
}
