// --
// katla is a data flow generator
// --
// Copyright (C) 2019 Twimpiex <twimpiex@gnx.be>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License version 3 as
// published by the Free Software Foundation.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.
// --
// Be aware that at any time this program may introduce bugs, backward
// incompatibilities, undesired behaviour, backdoors, ...
// Trust your testing and quality assurance, don't trust on semantic
// versioning.
//
// Contact the copyright owner if you wish to use this program with
// another license.
// --

use crate::model::context::Context;

use super::super::model::entity::Entity;
use super::entity_provider::EntityProvider;
use super::Identifier;

use serde::{Deserialize, Serialize};
use std::collections::HashMap;
use std::error::Error;
use std::{
    sync::mpsc::{channel, Receiver, Sender},
    //	thread,
    thread::JoinHandle,
};

#[derive(Serialize, Deserialize, Debug, Clone)]
#[serde(rename_all = "camelCase")]
pub struct Entry {
    pub name: String,
    pub entity: String,
}

impl Entry {
    pub fn start(&mut self, include_paths: Vec<String>) -> Result<EntryThread, Box<dyn Error>> {
        let identifier = self.get_identifier();
        let (entry_sender, entry_receiver): (Sender<EntryAction>, Receiver<EntryAction>) =
            channel();
        //let (result_sender, result_receiver): (Sender<EntryResult>, Receiver<EntryResult>) = channel();
        entry_sender.send(EntryAction::Start).unwrap();
        let mut entry = self.clone();

        let join_handle = std::thread::Builder::new()
            .name(identifier)
            .spawn(move || {
                let /*mut*/ count = 0;

                loop {
                    let entry_action = entry_receiver.recv().unwrap();
                    match entry_action.clone() {
                        EntryAction::Start => {}

                        EntryAction::Stats { result_sender } => {
                            result_sender
                                .send(EntryResult::Stats { count: count })
                                .unwrap();
                        }

                        EntryAction::Generate {
                            result_sender,
                            mut entity_providers,
                        } => {
                            result_sender
                                .send(
                                    entry
                                        .generate_entry(
                                            &mut entity_providers,
                                            &include_paths.iter().map(|s| s as &str).collect(),
                                        )
                                        .unwrap(),
                                )
                                .unwrap();
                        }

                        EntryAction::Stop => {
                            break;
                        }
                    }
                }
            })?;

        Ok(EntryThread {
            join_handle: Some(join_handle),
            sender: Some(entry_sender),
        })
    }

    fn generate_entry(
        &mut self,
        mut entity_providers: &mut HashMap<String, EntityProvider>,
        include_paths: &Vec<&str>,
    ) -> Result<Entity, Box<dyn Error>> {
        let mut entity = Entity::I8(0);
        let mut context = Context::entry();

        if let Some(mut entity_provider) = entity_providers.remove(&self.entity) {
            entity = entity_provider
                .get_entity(
                    &mut entity_providers,
                    &mut context,
                    include_paths,
                )
                .unwrap();
            entity_providers.insert(self.entity.to_string(), entity_provider);

            //entities.insert(
            //self.name.to_string(),
            //entity,
            //);
        }

        Ok(entity)
    }
}

impl Identifier for Entry {
    type Value = String;

    fn get_identifier(&self) -> String {
        self.name.to_string()
    }
}
#[derive(Debug)]
pub struct EntryThread {
    join_handle: Option<JoinHandle<()>>,
    pub sender: Option<Sender<EntryAction>>,
}

// Cloned EntryThread does not have the join_handle
impl Clone for EntryThread {
    fn clone(&self) -> EntryThread {
        EntryThread {
            join_handle: None,
            sender: self.sender.clone(),
        }
    }
}

impl EntryThread {
    pub fn stop(&mut self) -> Result<(), Box<dyn Error>> {
        self.sender
            .as_ref()
            .unwrap()
            .send(EntryAction::Stop)
            .unwrap();
        self.join_handle.take().map(JoinHandle::join);
        Ok(())
    }
}

#[derive(Debug, Clone)]
pub enum EntryAction {
    Start,
    Stats {
        result_sender: Sender<EntryResult>,
    },
    Generate {
        result_sender: Sender<Entity>,
        entity_providers: HashMap<String, EntityProvider>,
    },
    Stop,
}

#[derive(Debug, Clone)]
pub enum EntryResult {
    Stats { count: u64 },
    Entities { entities: HashMap<String, Entity> },
}
