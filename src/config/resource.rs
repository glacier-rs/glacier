// --
// katla is a data flow generator
// --
// Copyright (C) 2019 Twimpiex <twimpiex@gnx.be>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License version 3 as
// published by the Free Software Foundation.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.
// --
// Be aware that at any time this program may introduce bugs, backward
// incompatibilities, undesired behaviour, backdoors, ...
// Trust your testing and quality assurance, don't trust on semantic
// versioning.
//
// Contact the copyright owner if you wish to use this program with
// another license.
// --

use serde::{Deserialize, Serialize};
//use std::fmt::Display;
use std::str::FromStr;

use crate::serde::error::SerDeError;

// for now this is only a URL, should also support URN
// URL and URN are URI's
#[derive(Deserialize, Serialize, Debug, Clone, Eq, PartialEq)]
#[serde(tag = "scheme", rename_all = "camelCase")]
pub enum Resource {
    Data {
        // https://www.rfc-editor.org/rfc/rfc2397#section-2
        media_type: String,
        data: String,
    },
    File {
        media_type: Option<String>,
        authority: Option<String>,
        path: String,
    },
}

impl FromStr for Resource {
    type Err = SerDeError;

    fn from_str(uri: &str) -> Result<Self, Self::Err> {
        match uri.split_once(":") {
            Some((scheme, remaining)) => {
                match scheme {
                    "data" => {
                        // first parse data url, try to get a media type
                        let result: Result<(String, String), Self::Err> =
                            match remaining.split_once(",") {
                                Some((media_type, data)) => {
                                    Ok((media_type.to_string(), data.to_string()))
                                }
                                None => {
                                    // no media type and no ','
                                    Err(SerDeError::MissingCommaInUrl {
                                        url: uri.to_string(),
                                    })
                                }
                            };

                        if result.is_err() {
                            return Err(result.unwrap_err());
                        }

                        let media_type = if result.clone().unwrap().0.is_empty() {
                            "text/plain;charset=US-ASCII".to_string()
                        } else {
                            result.clone().unwrap().0
                        };
                        Ok(Resource::Data {
                            media_type: media_type,
                            data: result.clone().unwrap().1,
                        })
                    }
                    "file" => {
                        // https://datatracker.ietf.org/doc/html/rfc8089
                        // TODO split into authority and path
                        if remaining.starts_with("//") {
                            Ok(Resource::File {
                                media_type: None,
                                authority: Some("".to_string()),
                                path: remaining[2..].to_string(),
                            })
                        } else {
                            Ok(Resource::File {
                                media_type: None,
                                authority: None,
                                path: remaining.to_string(),
                            })
                        }
                    }
                    _ => Err(SerDeError::UnknownUrlScheme {
                        url: uri.to_string(),
                        scheme: scheme.to_string(),
                    }),
                }
            }
            None => Err(SerDeError::UnknownUri {
                uri: uri.to_owned(),
            }),
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_resource_data_from_str() {
        // data:[<mediatype>][;base64],<data>
        assert!(Resource::from_str("test data 1").is_err());
        assert!(Resource::from_str("data:test data 2").is_err());
        assert_eq!(
            Resource::from_str("data:,test data 3").unwrap(),
            Resource::Data {
                media_type: "text/plain;charset=US-ASCII".to_string(),
                data: "test data 3".to_string(),
            },
        );
        assert_eq!(
            Resource::from_str("data:application/x.katla-rs.rhai,test data 4").unwrap(),
            Resource::Data {
                media_type: "application/x.katla-rs.rhai".to_string(),
                data: "test data 4".to_string(),
            },
        );
        //assert_eq!(Resource::from_str("data:message/external-body;access-type=URL;URL=\"file:///path/to/directory\",Content-Type:%20application/x.katla-rs.rhai%0AContent-Transfer-Encoding:%20binary%0AContent-ID:%20<file://path/to/directory>").unwrap(), Eval::Rhai { inline: "test script 5".to_string(), file: None } );
    }

    #[test]
    fn test_resource_file_from_str() {
        assert_eq!(
            Resource::from_str("file:test file 1").unwrap(),
            Resource::File {
                media_type: None,
                authority: None,
                path: "test file 1".to_string(),
            },
        );

        //
        // https://datatracker.ietf.org/doc/html/rfc8089
        //
        // Local files:
        //
        // A traditional file URI for a local file with an empty authority.
        assert_eq!(
            Resource::from_str("file:///path/to/file").unwrap(),
            Resource::File {
                media_type: None,
                authority: Some("".to_string()),
                path: "/path/to/file".to_string(),
            }
        );

        // The minimal representation of a local file with no authority field
        // and an absolute path that begins with a slash "/".
        assert_eq!(
            Resource::from_str("file:/path/to/file").unwrap(),
            Resource::File {
                media_type: None,
                authority: None,
                path: "/path/to/file".to_string(),
            }
        );

        // The minimal representation of a local file in a DOS- or Windows-
        // based environment with no authority field and an absolute path
        // that begins with a drive letter.
        Resource::from_str("file:c:/path/to/file").unwrap();

        // Regular DOS or Windows file URIs with vertical line characters in the drive letter construct.
        Resource::from_str("file:///c|/path/to/file").unwrap();
        Resource::from_str("file:/c|/path/to/file").unwrap();
        Resource::from_str("file:c|/path/to/file").unwrap();

        // Non-local files:
        //
        // The representation of a non-local file with an explicit authority.
        Resource::from_str("file://host.example.com/path/to/file").unwrap();

        // The "traditional" representation of a non-local file with an empty
        // authority and a complete (transformed) UNC string in the path.
        Resource::from_str("file:////host.example.com/path/to/file").unwrap();

        // As above, with an extra slash between the empty authority and the transformed UNC string.
        Resource::from_str("file://///host.example.com/path/to/file").unwrap();
    }
}
