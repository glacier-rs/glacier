// --
// katla is a data flow generator
// --
// Copyright (C) 2019 Twimpiex <twimpiex@gnx.be>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License version 3 as
// published by the Free Software Foundation.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.
// --
// Be aware that at any time this program may introduce bugs, backward
// incompatibilities, undesired behaviour, backdoors, ...
// Trust your testing and quality assurance, don't trust on semantic
// versioning.
//
// Contact the copyright owner if you wish to use this program with
// another license.
// --

use super::eval::Eval;
use super::Identifier;
use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize, Debug, Clone)]
#[serde(tag = "type", rename_all = "camelCase")]
pub enum Feed {
    Csv {
        name: String,
        #[serde(deserialize_with = "super::super::serde::util::seq_string_or_map")]
        header: Vec<Eval>,
        #[serde(deserialize_with = "super::super::serde::util::seq_string_or_map")]
        record: Vec<Eval>,
        leading: Option<String>,
        trailing: Option<String>,
        delimiter: Option<String>,
    },
    File {
        name: String,
        #[serde(default)] //  This is needed for the optional to work
        #[serde(deserialize_with = "super::super::serde::util::option_string_or_map")]
        header: Option<Eval>,
        #[serde(default)] //  This is needed for the optional to work
        #[serde(deserialize_with = "super::super::serde::util::option_string_or_map")]
        #[serde(rename = "pageHeader")]
        page_header: Option<Eval>,
        #[serde(default)] //  This is needed for the optional to work
        #[serde(deserialize_with = "super::super::serde::util::option_string_or_map")]
        content: Option<Eval>,
        #[serde(default)] //  This is needed for the optional to work
        #[serde(deserialize_with = "super::super::serde::util::option_string_or_map")]
        #[serde(rename = "pageFooter")]
        page_footer: Option<Eval>,
        #[serde(default)] //  This is needed for the optional to work
        #[serde(deserialize_with = "super::super::serde::util::option_string_or_map")]
        footer: Option<Eval>,
        pagination: Pagination,
    },
    Json {
        name: String,
        content: Option<String>,
    },
}

#[derive(Serialize, Deserialize, Debug, Clone)]
#[serde(tag = "type", rename_all = "camelCase")]
pub enum Pagination {
    InFile { count: u64 },
    PerFile { count: u64 },
}

impl Identifier for Feed {
    type Value = String;

    fn get_identifier(&self) -> String {
        match self {
            Feed::Csv { name, .. } => name.clone(),
            Feed::File { name, .. } => name.clone(),
            Feed::Json { name, .. } => name.clone(),
        }
    }
}
