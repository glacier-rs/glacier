// --
// katla is a data flow generator
// --
// Copyright (C) 2019 Twimpiex <twimpiex@gnx.be>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License version 3 as
// published by the Free Software Foundation.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.
// --
// Be aware that at any time this program may introduce bugs, backward
// incompatibilities, undesired behaviour, backdoors, ...
// Trust your testing and quality assurance, don't trust on semantic
// versioning.
//
// Contact the copyright owner if you wish to use this program with
// another license.
// --

// katla::config::time::interval

use crate::serde::error::SerDeError;

use super::deviation::Deviation;
use super::duration::Duration;
use chrono::DateTime;
//use chrono::Utc;
use chrono::FixedOffset;
use serde::{Deserialize, Serialize};
//use std::time::Instant;
use std::str::FromStr;
//use regex::Regex;
//use std::time::Duration as StdTimeDuration;

// ISO 8601 Time Interval
#[derive(Deserialize, Serialize, Debug, Default, Clone, Eq, PartialEq)]
#[serde(rename_all = "camelCase")]
pub struct Interval {
    pub repeating: bool,
    pub repetitions: Option<u64>,
    #[serde(with = "serde_datetime")]
    pub start: Option<DateTime<FixedOffset>>,
    pub duration: Option<Duration>,
    #[serde(with = "serde_datetime")]
    pub end: Option<DateTime<FixedOffset>>,
}

impl FromStr for Interval {
    type Err = SerDeError;

    fn from_str(string: &str) -> Result<Self, Self::Err> {
        let parts = string.split("/").collect::<Vec<&str>>();
        let mut part_index = 0;
        let mut repeating = false;
        let mut repetitions = None;
        let mut start = None;
        let mut duration = None;
        let mut end = None;

        if part_index < parts.len() && parts[part_index].starts_with("R") {
            repeating = true;
            if parts[part_index].len() > 1 {
                let count = parts[part_index][1..].parse::<i64>().unwrap();
                match count {
                    0 => {
                        repeating = false;
                        repetitions = None;
                    }
                    -1 => {
                        repetitions = None;
                    }
                    _ => repetitions = Some(count as u64),
                }
            }
            part_index += 1;
        }
        while part_index < parts.len() {
            if parts[part_index].starts_with("P") {
                // duration
                duration = Some(Duration::from(parts[part_index]));
            } else {
                // either start or end
                match start {
                    Some(_) => {
                        end = Some(DateTime::parse_from_rfc3339(parts[part_index]).unwrap());
                    }
                    None => match duration {
                        Some(_) => {
                            end = Some(DateTime::parse_from_rfc3339(parts[part_index]).unwrap());
                        }
                        None => {
                            start = Some(DateTime::parse_from_rfc3339(parts[part_index]).unwrap());
                        }
                    },
                }
            }
            part_index += 1;
        }

        Ok(Interval {
            repeating: repeating,
            repetitions: repetitions,
            start: start,
            duration: duration,
            end: end,
        })
    }
}

impl From<&str> for Interval {
    fn from(string: &str) -> Self {
        Interval::from_str(string).unwrap()
    }
}

// Interval with Deviation
#[derive(Deserialize, Serialize, Debug, Default, Clone, Eq, PartialEq)]
#[serde(rename_all = "camelCase")]
pub struct DeviatedInterval {
    #[serde(deserialize_with = "super::super::super::serde::util::option_string_or_map")]
    pub interval: Option<Interval>,
    pub deviation: Option<Deviation>,
}

impl FromStr for DeviatedInterval {

    type Err = SerDeError;

    fn from_str(string: &str) -> Result<Self, Self::Err> {
        let parts = string.rsplitn(2, "/X").collect::<Vec<&str>>();
        let mut deviation = None;
        if parts.len() == 2 {
            deviation = Some((parts[1].to_string(), 0));
        }

        let interval = Some(Interval::from(parts[0]));

        Ok(DeviatedInterval {
            interval: interval,
            deviation: deviation,
        })
    }
}

impl From<&str> for DeviatedInterval {
    fn from(string: &str) -> Self {
        DeviatedInterval::from_str(string).unwrap()
    }
}

mod serde_datetime {
    use chrono::DateTime;
    use chrono::FixedOffset;
    use serde::{de::Error, Deserialize, Deserializer, Serialize, Serializer};

    pub fn serialize<S: Serializer>(
        time: &Option<DateTime<FixedOffset>>,
        serializer: S,
    ) -> Result<S::Ok, S::Error> {
        match time {
            Some(time) => time.to_rfc3339().serialize(serializer),
            None => "".serialize(serializer),
        }
    }

    pub fn deserialize<'de, D: Deserializer<'de>>(
        deserializer: D,
    ) -> Result<Option<DateTime<FixedOffset>>, D::Error> {
        let time: String = Deserialize::deserialize(deserializer)?;
        let time_str: &str = &time;
        match time_str {
            "" => Ok(None),
            _ => Ok(Some(
                DateTime::parse_from_rfc3339(&time).map_err(D::Error::custom)?,
            )),
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_interval_from_repetitions_start_duration() {
        let interval = Interval::from("R5/2008-03-01T13:00:00Z/P1Y2M10DT2H30M");
        assert_eq!(interval.repeating, true);
        assert_eq!(interval.repetitions, Some(5));
        assert_eq!(
            interval.start,
            Some(DateTime::parse_from_rfc3339("2008-03-01T13:00:00Z").unwrap())
        );
        //assert_eq!(interval.duration, Some(Duration::years(1).add(Duration::months(2)).days(Duration::months(10)).add(Duration::hours(2)).add(Duration::minutes(30))));
        assert_eq!(interval.duration, Some(Duration::from("P1Y2M10DT2H30M")));
        assert_eq!(interval.end, None);
    }

    #[test]
    fn test_interval_from_repetitions_duration_end() {
        let interval = Interval::from("R5/P1Y2M10DT2H30M/2008-03-01T13:00:00Z");
        assert_eq!(interval.repeating, true);
        assert_eq!(interval.repetitions, Some(5));
        assert_eq!(interval.start, None);
        //assert_eq!(interval.duration, Some(Duration::years(1).add(Duration::months(2)).days(Duration::months(10)).add(Duration::hours(2)).add(Duration::minutes(30))));
        assert_eq!(interval.duration, Some(Duration::from("P1Y2M10DT2H30M")));
        assert_eq!(
            interval.end,
            Some(DateTime::parse_from_rfc3339("2008-03-01T13:00:00Z").unwrap())
        );
    }

    #[test]
    fn test_interval_from_repetitions_start_end() {
        let interval = Interval::from("R5/2008-03-01T13:00:00Z/2008-04-01T13:00:00Z");
        assert_eq!(interval.repeating, true);
        assert_eq!(interval.repetitions, Some(5));
        assert_eq!(
            interval.start,
            Some(DateTime::parse_from_rfc3339("2008-03-01T13:00:00Z").unwrap())
        );
        assert_eq!(interval.duration, None);
        assert_eq!(
            interval.end,
            Some(DateTime::parse_from_rfc3339("2008-04-01T13:00:00Z").unwrap())
        );
    }

    #[test]
    fn test_interval_from_repetitions_duration() {
        let interval = Interval::from("R5/P1Y2M10DT2H30M");
        assert_eq!(interval.repeating, true);
        assert_eq!(interval.repetitions, Some(5));
        assert_eq!(interval.start, None);
        //assert_eq!(interval.duration, Some(Duration::years(1).add(Duration::months(2)).days(Duration::months(10)).add(Duration::hours(2)).add(Duration::minutes(30))));
        assert_eq!(interval.duration, Some(Duration::from("P1Y2M10DT2H30M")));
        assert_eq!(interval.end, None);
    }

    #[test]
    fn test_interval_from_0_repetitions_start_end() {
        let interval = Interval::from("R0/2008-03-01T13:00:00Z/2008-04-01T13:00:00Z");
        //assert_eq!(interval.repeating, false);
        assert_eq!(interval.repetitions, None);
        assert_eq!(
            interval.start,
            Some(DateTime::parse_from_rfc3339("2008-03-01T13:00:00Z").unwrap())
        );
        assert_eq!(interval.duration, None);
        assert_eq!(
            interval.end,
            Some(DateTime::parse_from_rfc3339("2008-04-01T13:00:00Z").unwrap())
        );
    }

    #[test]
    fn test_interval_from_m1_repetitions_start_end() {
        let interval = Interval::from("R-1/2008-03-01T13:00:00Z/2008-04-01T13:00:00Z");
        assert_eq!(interval.repeating, true);
        assert_eq!(interval.repetitions, None);
        assert_eq!(
            interval.start,
            Some(DateTime::parse_from_rfc3339("2008-03-01T13:00:00Z").unwrap())
        );
        assert_eq!(interval.duration, None);
        assert_eq!(
            interval.end,
            Some(DateTime::parse_from_rfc3339("2008-04-01T13:00:00Z").unwrap())
        );
    }

    #[test]
    fn test_interval_from_unbounded_repetitions_start_end() {
        let interval = Interval::from("R/2008-03-01T13:00:00Z/2008-04-01T13:00:00Z");
        assert_eq!(interval.repeating, true);
        assert_eq!(interval.repetitions, None);
        assert_eq!(
            interval.start,
            Some(DateTime::parse_from_rfc3339("2008-03-01T13:00:00Z").unwrap())
        );
        assert_eq!(interval.duration, None);
        assert_eq!(
            interval.end,
            Some(DateTime::parse_from_rfc3339("2008-04-01T13:00:00Z").unwrap())
        );
    }

    #[test]
    fn test_interval_from_start_end() {
        let interval = Interval::from("2008-03-01T13:00:00Z/2008-04-01T13:00:00Z");
        assert_eq!(interval.repeating, false);
        assert_eq!(interval.repetitions, None);
        assert_eq!(
            interval.start,
            Some(DateTime::parse_from_rfc3339("2008-03-01T13:00:00Z").unwrap())
        );
        assert_eq!(interval.duration, None);
        assert_eq!(
            interval.end,
            Some(DateTime::parse_from_rfc3339("2008-04-01T13:00:00Z").unwrap())
        );
    }

    #[test]
    fn test_deviated_interval_from_repetitions_start_duration() {
        // TODO extension for deviation
        let deviated_interval = DeviatedInterval::from("R5/2008-03-01T13:00:00Z/P1Y2M10DT2H30M");
        assert_eq!(
            deviated_interval.interval,
            Some(Interval::from("R5/2008-03-01T13:00:00Z/P1Y2M10DT2H30M"))
        );
        assert_eq!(deviated_interval.deviation, None);
	}
}
