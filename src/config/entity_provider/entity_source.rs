// --
// katla is a data flow generator
// --
// Copyright (C) 2019 Twimpiex <twimpiex@gnx.be>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License version 3 as
// published by the Free Software Foundation.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.
// --
// Be aware that at any time this program may introduce bugs, backward
// incompatibilities, undesired behaviour, backdoors, ...
// Trust your testing and quality assurance, don't trust on semantic
// versioning.
//
// Contact the copyright owner if you wish to use this program with
// another license.
// --

use serde::Deserialize;
use serde::Serialize;

#[derive(Serialize, Deserialize, Debug, Clone)]
#[serde(tag = "type", rename_all = "camelCase")]
pub enum EntitySource {
    Csv {
        location: String,
        entities: String,
        weights: String,
        #[serde(rename = "withHeaders")]
        with_headers: bool,
        totals: String,
    },
    SpreadSheet {
        // xlsx+file://./inventory/Familienamen_2019.xlsx
        location: String,
        // B1:B200
        // C
        // C3:
        // 5 (row 5)
        // top10000_2019.B1:B200
        // top10000_2019.C
        // top10000_2019.C3:
        // top10000_2019.B1:B200,sheet_name.C4:C6 -> union of these two
        entities: String,
        weights: String,
        #[serde(rename = "withHeaders")]
        with_headers: bool,
        // first
        // last
        // none
        totals: String,
    },
    Rng {
        #[serde(rename = "dataType")]
        data_type: String,
        range: Option<RngRange>,
    },
}

#[derive(Serialize, Deserialize, Debug, Clone)]
#[serde(rename_all = "camelCase")]
pub struct RngRange {
    pub low: f64,
    pub high: f64,
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_source_serialize_csv_weights_with_headers_totals() {
        let source = EntitySource::Csv {
            location: "file://csv_weights_with_headers_totals.csv".to_string(),
            entities: "B2:B".to_string(),
            weights: "A2:A".to_string(),
            with_headers: true,
            totals: "C2:C".to_string(),
        };

        assert_eq!(
            serde_json::to_string(&source).unwrap(),
            "{\"type\":\"csv\",\"location\":\"file://csv_weights_with_headers_totals.csv\",\"entities\":\"B2:B\",\"weights\":\"A2:A\",\"withHeaders\":true,\"totals\":\"C2:C\"}"
        );

        assert_eq!(
            ron::to_string(&source).unwrap(),
            "(type:\"csv\",location:\"file://csv_weights_with_headers_totals.csv\",entities:\"B2:B\",weights:\"A2:A\",withHeaders:true,totals:\"C2:C\")"
        );
    }

    #[test]
    fn test_source_deserialize_csv_weights_with_headers_totals() {
        let source: EntitySource = ron::from_str(
            &"
(
    type: \"csv\",
    location: \"file://csv_weights_with_headers_totals.csv\",
    entities: \"C2:C\",
    weights: \"A2:A\",
    withHeaders: true,
    totals: \"none\",
)",
        )
        .unwrap();

        match source {
            EntitySource::Csv {
                location,
                entities,
                weights,
                with_headers,
                totals,
            } => {
                assert_eq!(location, "file://csv_weights_with_headers_totals.csv");
                assert_eq!(entities, "C2:C");
                assert_eq!(weights, "A2:A");
                assert_eq!(with_headers, true);
                assert_eq!(totals, "none");
            }
            _ => assert!(false),
        }
    }
}
