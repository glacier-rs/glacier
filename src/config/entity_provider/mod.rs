// --
// katla is a data flow generator
// --
// Copyright (C) 2019 Twimpiex <twimpiex@gnx.be>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License version 3 as
// published by the Free Software Foundation.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.
// --
// Be aware that at any time this program may introduce bugs, backward
// incompatibilities, undesired behaviour, backdoors, ...
// Trust your testing and quality assurance, don't trust on semantic
// versioning.
//
// Contact the copyright owner if you wish to use this program with
// another license.
// --

pub mod entity_source;

use self::entity_source::EntitySource;

use super::eval::Eval;
use super::Identifier;
use serde::Deserialize;
use serde::Serialize;
use std::sync::Arc;
use std::sync::Mutex;

#[derive(Serialize, Deserialize, /*Serialize,*/ Debug, Clone)]
#[serde(tag = "type", rename_all = "camelCase")]
pub enum EntityProvider {
    Sequence {
        name: String,
        start: Option<i64>,
        hold: Option<i64>,
        increment: Option<i64>,
        minimum: Option<i64>,
        maximum: Option<i64>,
        cycle: Option<bool>,
        #[serde(skip)]
        sequence: Arc<Mutex<i64>>,
        #[serde(skip)]
        init: Arc<Mutex<bool>>,
        #[serde(skip)]
        counter: Arc<Mutex<i64>>,
    },
    Timestamp {
        name: String,
    },
    Map {
        name: String,
        map: Vec<MapEntityProviderElement>,
    },
    Random {
        name: String,
        #[serde(rename = "arraySize")]
        array_size: Option<usize>,
        // ie xlsx+file://./inventory/Familienamen_2019.xlsx{"sheet":"top10000_2019","withHeaders":true,"totals":"last","entities":"[B1-B200]","weights":"[C]"}
        uri: Option<String>,
        source: EntitySource,
    },
    Uuid {
        name: String,
    },
    Eval {
        name: String,
        #[serde(deserialize_with = "super::super::serde::util::string_or_map")]
        expression: Eval,
    },
}

#[derive(Serialize, Deserialize, Debug, Clone)]
#[serde(rename_all = "camelCase")]
pub struct MapEntityProviderElement {
    pub field: String,
    pub entity: String,
}

impl Identifier for EntityProvider {
    type Value = String;

    fn get_identifier(&self) -> String {
        match self {
            EntityProvider::Sequence { name, .. } => name.to_string(),

            EntityProvider::Timestamp { name, .. } => name.to_string(),

            EntityProvider::Map { name, .. } => name.to_string(),

            EntityProvider::Random { name, .. } => name.to_string(),

            EntityProvider::Uuid { name, .. } => name.to_string(),

            EntityProvider::Eval { name, .. } => name.to_string(),
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_sequence_serialize() {
        let sequence = EntityProvider::Sequence {
            name: "name-identifier".to_string(),
            start: None,
            hold: None,
            increment: None,
            minimum: None,
            maximum: None,
            cycle: None,
            sequence: Arc::new(Mutex::default()),
            init: Arc::new(Mutex::default()),
            counter: Arc::new(Mutex::default()),
        };

        assert_eq!(
            serde_json::to_string(&sequence).unwrap(),
            "{\"type\":\"sequence\",\"name\":\"name-identifier\",\"start\":null,\"hold\":null,\"increment\":null,\"minimum\":null,\"maximum\":null,\"cycle\":null}"
        );
    }

    #[test]
    fn test_sequence_deserialize() {
        // bare minimum sequence
        let sequence: EntityProvider = ron::from_str(
            &"(
	type: \"sequence\",
	name: \"name-identifier\",
)",
        )
        .unwrap();

        match sequence {
            EntityProvider::Sequence {
                name,
                start,
                hold,
                increment,
                minimum,
                maximum,
                cycle,
                ..
            } => {
                assert_eq!(name, "name-identifier");
                assert_eq!(start, None);
                assert_eq!(hold, None);
                assert_eq!(increment, None);
                assert_eq!(minimum, None);
                assert_eq!(maximum, None);
                assert_eq!(cycle, None);
            }
            _ => assert!(false),
        }

        // populated sequence
        let sequence: EntityProvider = ron::from_str(
            &"(
	type: \"sequence\",
	name: \"name-identifier\",
	start: 1,
	hold: 2,
	increment: 3,
	minimum: -1,
	maximum: -2,
	cycle: false,
)",
        )
        .unwrap();

        match sequence {
            EntityProvider::Sequence {
                name,
                start,
                hold,
                increment,
                minimum,
                maximum,
                cycle,
                ..
            } => {
                assert_eq!(name, "name-identifier");
                assert_eq!(start, Some(1));
                assert_eq!(hold, Some(2));
                assert_eq!(increment, Some(3));
                assert_eq!(minimum, Some(-1));
                assert_eq!(maximum, Some(-2));
                assert_eq!(cycle, Some(false));
            }
            _ => assert!(false),
        }
    }
}
