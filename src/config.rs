// --
// katla is a data flow generator
// --
// Copyright (C) 2019 Twimpiex <twimpiex@gnx.be>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License version 3 as
// published by the Free Software Foundation.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.
// --
// Be aware that at any time this program may introduce bugs, backward
// incompatibilities, undesired behaviour, backdoors, ...
// Trust your testing and quality assurance, don't trust on semantic
// versioning.
//
// Contact the copyright owner if you wish to use this program with
// another license.
// --

// TODO config namespace should only contain code that defines 'external' api, so it should also be semver compatibile

pub mod appender;
pub mod entity_provider;
pub mod entry;
pub mod eval;
pub mod feed;
pub mod flow;
pub mod time;
pub mod resource;

use super::entity_provider::EntityProviderThread;
use appender::Appender;
use entity_provider::EntityProvider;
use entry::Entry;
use entry::EntryThread;
use feed::Feed;
use flow::Flow;
use flow::FlowStat;
use flow::FlowThread;

use serde::Deserialize;
use std::collections::HashMap;
use std::error::Error;
use std::fs::File;
use std::io::prelude::*;
use std::path::PathBuf;
use std::sync::mpsc::{channel, Receiver, Sender};

use crate::appender::AppenderThread;

#[derive(Deserialize, Debug)]
#[serde(rename_all = "camelCase")]
pub struct Katla {
    #[serde(default)] //  This is needed for being optional/empty
    pub name: String,
    #[serde(default)] //  This is needed for being optional/empty
    pub description: String,
    #[serde(default)] //  This is needed for being optional/empty
    pub imports: Vec<String>,
    #[serde(default)] //  This is needed for being optional/empty
    #[serde(deserialize_with = "super::serde::util::array_of_struct_with_identifier_as_map")]
    pub entity_providers: HashMap<String, EntityProvider>,
    #[serde(default)] //  This is needed for being optional/empty
    #[serde(deserialize_with = "super::serde::util::array_of_struct_with_identifier_as_map")]
    pub appenders: HashMap<String, Appender>,
    #[serde(default)] //  This is needed for being optional/empty
    #[serde(deserialize_with = "super::serde::util::array_of_struct_with_identifier_as_map")]
    pub flows: HashMap<String, Flow>,
    #[serde(default)] //  This is needed for being optional/empty
    #[serde(deserialize_with = "super::serde::util::array_of_struct_with_identifier_as_map")]
    pub feeds: HashMap<String, Feed>,
    #[serde(default)] //  This is needed for being optional/empty
    pub entries: Vec<Entry>,

    #[serde(skip)]
    pub include_paths: Option<Vec<String>>,
    #[serde(skip)]
    pub stat_receiver: Option<Receiver<FlowStat>>,
    #[serde(skip)]
    pub appender_threads: HashMap<String, AppenderThread>,
    #[serde(skip)]
    pub entity_provider_threads: HashMap<String, EntityProviderThread>,
    #[serde(skip)]
    pub entry_threads: HashMap<String, EntryThread>,
    #[serde(skip)]
    pub flow_threads: HashMap<String, FlowThread>,
}

impl Katla {
    pub fn new(name: String, description: String) -> Katla {
        Katla {
            name: name,
            description: description,
            imports: Vec::new(),
            entity_providers: HashMap::new(),
            entries: Vec::new(),
            appenders: HashMap::new(),
            feeds: HashMap::new(),
            flows: HashMap::new(),

            include_paths: Some(Vec::new()),
            stat_receiver: None,
            appender_threads: HashMap::new(),
            entity_provider_threads: HashMap::new(),
            entry_threads: HashMap::new(),
            flow_threads: HashMap::new(),
        }
    }

    pub fn new_from_config(
        config: String,
        include_paths: &Vec<&str>,
    ) -> Result<Katla, Box<dyn Error>> {
        let mut katla = Katla::new("Unknown".to_string(), "Unknown".to_string());
        katla.include_paths = Some(include_paths.iter().map(|s| s.to_string()).collect());
        katla.import_config(config, include_paths).unwrap();
        Ok(katla)
    }

    pub fn path_on_include_path(filename: String, include_paths: &Vec<&str>) -> PathBuf {
        let path = PathBuf::from(filename.to_string());
        if path.is_file() {
            return path;
        } else {
            for include_path in include_paths {
                let mut path = PathBuf::from(include_path.to_string());
                path.push(filename.to_string());
                if path.is_file() {
                    return path;
                }
            }
        }
        return path;
    }

    pub fn load_config(
        &mut self,
        config: String,
        include_paths: &Vec<&str>,
    ) -> Result<&mut Katla, Box<dyn Error>> {
        self.include_paths = Some(include_paths.iter().map(|s| s.to_string()).collect());
        self.import_config(config, include_paths).unwrap();
        Ok(self)
    }

    pub fn import_config(
        &mut self,
        config: String,
        include_paths: &Vec<&str>,
    ) -> Result<(), Box<dyn Error>> {
        debug!("config = {:#?}", config);
        // TODO better regex match for json/ron/... any future config file format
        let import_katla: Katla = match config.chars().next() {
            Some('{') => serde_json::from_str(&config)
                .expect(&(format!("{}{}", "Unable to parse ".to_owned(), config.to_owned()))),
            // starting with ( or struct name
            _ => ron::from_str(&config)
                .expect(&(format!("{}{}", "Unable to parse ".to_owned(), config.to_owned()))),
        };
        debug!("import = {:#?}", import_katla);

        for import in import_katla.imports {
            let mut katla_import_cfg_string = String::new();
            let mut katla_import_cfg_file = File::open(Katla::path_on_include_path(
                import.to_string(),
                include_paths,
            ))
            .expect(&(format!("{}{}", "Unable to open file ".to_owned(), import.to_owned())));
            katla_import_cfg_file
                .read_to_string(&mut katla_import_cfg_string)
                .expect("Unable to read the file");
            self.import_config(katla_import_cfg_string, include_paths)
                .unwrap();
        }

        for (_identifier, entity_provider) in import_katla.entity_providers {
            self.add_entity_provider(entity_provider);
        }

        for entry in import_katla.entries {
            self.add_entry(entry);
        }

        for (_identifier, appender) in import_katla.appenders {
            self.add_appender(appender);
        }

        for (_identifier, flow) in import_katla.flows {
            self.add_flow(flow);
        }

        for (_identifier, feed) in import_katla.feeds {
            self.add_feed(feed);
        }

        self.name = import_katla.name.to_string();
        self.description = import_katla.description.to_string();
        Ok(())
    }

    pub fn generate(&mut self) -> Result<(), Box<dyn Error>> {
        println!("generate");
        println!("entity_providers {}", self.entity_providers.len());
        println!("appenders {}", self.appenders.len());
        println!("entries {}", self.entries.len());
        println!("flows {}", self.flows.len());
        println!("feeds {}", self.feeds.len());
        self.start_entity_providers()?;
        self.start_appenders()?;
        self.start_entries()?;
        self.start_flows()?;
        self.stats();

        self.stop_flows()?;
        self.stop_entries()?;
        self.stop_appenders()?;
        self.stop_entity_providers()?;
        Ok(())
    }

    fn stats(&mut self) {
        let mut stat_identifiers = Vec::new();
        let mut stats_map = HashMap::new();

        while let Ok(stat) = self.stat_receiver.as_ref().unwrap().recv() {
            //print!("\x1b[1G");
            print!("\x1b[{}A", stat_identifiers.len());

            if !stat_identifiers.contains(&stat.flow_identifier.to_string()) {
                stat_identifiers.push(stat.flow_identifier.to_string());
            }
            stats_map.insert(stat.flow_identifier.to_string(), stat.clone());

            for stat_identifier in &stat_identifiers {
                let stat_print = stats_map.get(stat_identifier).unwrap();
                if stat_print.start_instant.elapsed().as_secs() == 0
                    || (stat_print.start_instant.elapsed().as_secs()
                        == stat_print.update_instant.elapsed().as_secs())
                {
                    println!(
                        "{:#?}\t{:3}% {:#?} {:#?}/{:#?}",
                        stat_identifier, 0, 0, 0, stat_print.total
                    );
                } else {
                    println!(
                        "{:#?}\t{:3}% {:#?} {:#?}/{:#?}",
                        stat_identifier,
                        stat_print.done * 100 / stat_print.total,
                        stat_print.done
                            / (stat_print.start_instant.elapsed().as_secs()
                                - stat_print.update_instant.elapsed().as_secs()),
                        stat_print.done,
                        stat_print.total
                    );
                }
            }

            //io::stdout().flush().ok().expect("Could not flush stdout");
        }
    }

    fn start_entity_providers(&mut self) -> Result<(), Box<dyn Error>> {
        for (identifier, entity_provider) in self.entity_providers.iter_mut() {
            self.entity_provider_threads
                .insert(identifier.to_string(), entity_provider.start().unwrap());
        }

        Ok(())
    }

    fn stop_entity_providers(&mut self) -> Result<(), Box<dyn Error>> {
        for (_identifier, entity_provider_thread) in self.entity_provider_threads.iter_mut() {
            entity_provider_thread.stop()?;
        }

        Ok(())
    }

    fn start_appenders(&mut self) -> Result<(), Box<dyn Error>> {
        for (identifier, appender) in self.appenders.iter_mut() {
            self.appender_threads
                .insert(identifier.to_string(), appender.start().unwrap());
        }
        // some appenders need other appenders communication channels
        for (_identifier, appender_thread) in self.appender_threads.clone().iter_mut() {
            appender_thread
                .appenders(self.appender_threads.clone())
                .unwrap();
        }

        Ok(())
    }

    fn stop_appenders(&mut self) -> Result<(), Box<dyn Error>> {
        for (_identifier, appender_thread) in self.appender_threads.iter_mut() {
            appender_thread.stop()?;
        }

        Ok(())
    }

    fn start_flows(&mut self) -> Result<(), Box<dyn Error>> {
        let (result_sender, result_receiver): (Sender<FlowStat>, Receiver<FlowStat>) = channel();
        self.stat_receiver = Some(result_receiver);
        for (identifier, flow) in self.flows.iter_mut() {
            self.flow_threads.insert(
                identifier.to_string(),
                flow.start(
                    result_sender.clone(),
                    self.entity_providers.clone(),
                    self.entry_threads.clone(),
                    self.feeds.clone(),
                    self.appender_threads.clone(),
                )
                .unwrap(),
            );
        }

        Ok(())
    }

    fn stop_flows(&mut self) -> Result<(), Box<dyn Error>> {
        for (_identifier, flow_thread) in self.flow_threads.iter_mut() {
            flow_thread.stop()?;
        }

        Ok(())
    }

    fn start_entries(&mut self) -> Result<(), Box<dyn Error>> {
        for entry in self.entries.iter_mut() {
            self.entry_threads.insert(
                entry.get_identifier().to_string(),
                entry
                    .start(self.include_paths.clone().unwrap().clone())
                    .unwrap(),
            );
        }

        Ok(())
    }

    fn stop_entries(&mut self) -> Result<(), Box<dyn Error>> {
        for (_identifier, entry_thread) in self.entry_threads.iter_mut() {
            entry_thread.stop()?;
        }

        Ok(())
    }

    pub fn add_entity_provider(&mut self, entity_provider: EntityProvider) {
        self.entity_providers
            .insert(entity_provider.get_identifier(), entity_provider);
    }

    pub fn add_appender(&mut self, appender: Appender) {
        self.appenders.insert(appender.get_identifier(), appender);
    }

    pub fn add_entry(&mut self, entry: Entry) {
        self.entries.push(entry);
    }

    pub fn add_flow(&mut self, flow: Flow) {
        self.flows.insert(flow.get_identifier(), flow);
    }

    pub fn add_feed(&mut self, feed: Feed) {
        self.feeds.insert(feed.get_identifier(), feed);
    }
}

pub trait Identifier {
    type Value;

    fn get_identifier(&self) -> Self::Value;
}

impl PartialEq for dyn Identifier<Value = String> {
    fn eq(&self, other: &Self) -> bool {
        self.get_identifier() == other.get_identifier()
    }
}
impl Eq for dyn Identifier<Value = String> {}
