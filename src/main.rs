// --
// katla is a data flow generator
// --
// Copyright (C) 2019 Twimpiex <twimpiex@gnx.be>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License version 3 as
// published by the Free Software Foundation.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.
// --
// Be aware that at any time this program may introduce bugs, backward
// incompatibilities, undesired behaviour, backdoors, ...
// Trust your testing and quality assurance, don't trust on semantic
// versioning.
//
// Contact the copyright owner if you wish to use this program with
// another license.
// --
#[macro_use]
extern crate log;

use env_logger;

use katla::config::*;

use std::io::prelude::*;
use std::fs::File;
use std::process;

use clap::Parser;

#[derive(Parser, Debug, Clone)]
#[command(author, version, about, long_about = None)]
struct CommandLineArgumentsOptions {
	/// Enable profiler
	#[clap(short)]
	profiler: bool,

	/// Enable GUI
	#[clap(short)]
	gui: bool,

	/// orbtk GUI
	#[clap(long)]
	orbtk: bool,

	/// gtk-rs GUI
	#[clap(long)]
	gtk: bool,

	/// web GUI
	#[clap(long)]
	web: bool,

	#[clap(short, long, default_value = ".")]
	include_path: String,

	katla_file: String,

    #[arg(short, long, default_value_t = false)]
    deamon: bool,
}

struct CommandLine {
	clao: Option<CommandLineArgumentsOptions>,
}

impl Default for CommandLine {
	fn default() -> CommandLine {
		CommandLine {
			clao: None,
		}
	}
}

impl CommandLine {
	fn execute(&mut self) {
		let include_path_entries = &(self.clao.as_ref().unwrap().include_path.split(":").collect());

		let mut katla_cfg_file = File::open(Katla::path_on_include_path(self.clao.as_ref().unwrap().katla_file.to_string(), include_path_entries)).expect(&(format!("{}{}", "Unable to open file ".to_owned(), self.clao.as_ref().unwrap().katla_file.to_string())));
		let mut katla_cfg_string = String::new();
		katla_cfg_file.read_to_string(&mut katla_cfg_string).expect("Unable to read the file");

		let mut katla = Katla::new("name".to_owned(), "description".to_owned());
		katla.load_config(katla_cfg_string, include_path_entries).unwrap();
		//katla.run().unwrap();
//			let mut katla = Katla::load_from_config().unwrap();

		if 	cfg!(any(feature="gui-orbtk", feature="gui-gtk")) && self.clao.as_ref().unwrap().gui {
			if cfg!(feature="gui-gtk") && self.clao.as_ref().unwrap().gtk {
				#[cfg(feature="gui-gtk")]
				self.gtk_gui();
			} else if cfg!(feature="gui-orbtk") && self.clao.as_ref().unwrap().orbtk {
				#[cfg(feature="gui-orbtk")]
				self.orbtk_gui();
			} else {
				#[cfg(feature="gui-orbtk")]
				self.orbtk_gui();
			}
		} else {
			if let Err(err) = katla.generate() {
				debug!("{:#?}", err);
				process::exit(1);
			}
		}

	}

	#[cfg(feature="gui-orbtk")]
	fn orbtk_gui(&mut self) {
		use orbtk::prelude::*;
		Application::new().window(|ctx| {
			Window::new()
				.title("Katla")
				.position((100.0, 100.0))
				.size(420.0, 730.0)
				.child(TextBlock::new().text("OrbTk").build(ctx))
				.build(ctx)
		}).run();
	}

	#[cfg(feature="gui-gtk")]
	fn gtk_gui(&mut self) {
	}

}

//#[async_std::main]
//#[actix_web::main]
/* async */ fn main() /* -> std::io::Result<()> */ {
    env_logger::init();
	#[cfg(feature="profiler")]
	use cpuprofiler::PROFILER;

	let clao: CommandLineArgumentsOptions = CommandLineArgumentsOptions::parse();

	#[cfg(feature="profiler")]
	if clao.clone().profiler {
		PROFILER.lock().unwrap().start("./katla.profile").unwrap();
	}
	
    //let args: Vec<String> = std::env::args().collect();
    //let program_name = args[0].clone();
    //println!("program_name  {:#?}", program_name);

    let mut command_line = CommandLine { clao: Some(clao.clone()), ..Default::default() };

    command_line.execute();

	#[cfg(feature="profiler")]
	if command_line_options.clone().profiler {
		PROFILER.lock().unwrap().stop().unwrap();
	}

	//Ok(())
}
