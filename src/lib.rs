// --
// katla is a data flow generator
// --
// Copyright (C) 2019 Twimpiex <twimpiex@gnx.be>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License version 3 as
// published by the Free Software Foundation.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.
// --
// Be aware that at any time this program may introduce bugs, backward
// incompatibilities, undesired behaviour, backdoors, ...
// Trust your testing and quality assurance, don't trust on semantic
// versioning.
//
// Contact the copyright owner if you wish to use this program with
// another license.
// --
pub mod appender;
pub mod config;
pub mod csv;
pub mod eval;
pub mod entity_provider;
pub mod feed;
pub mod model;
pub mod resource;
pub mod serde;

use config::*;

#[macro_use]
extern crate log;
extern crate csv as extcsv;
extern crate rand;
extern crate regex;
#[macro_use]
extern crate lazy_static;

//
// c/java api
//
#[cfg(feature="java")]
use jni::objects::{JClass, JObject, JString};
#[cfg(feature="java")]
use jni::sys::jobject;
#[cfg(feature="java")]
use jni::JNIEnv;

#[cfg(feature="java")]
#[no_mangle]
pub extern "system" fn Java_Katla_create(
    mut env: JNIEnv,
    class: JClass,
    name: &JString,
    description: &JString,
) -> jobject {
    let output = env.new_object(class, "()V", &[]).unwrap();
    let katla = Katla::new(
        String::from(env.get_string(name).unwrap()),
        String::from(env.get_string(description).unwrap()),
    );
    //env.set_rust_field(output, "katlaReference", katla).unwrap();
    output.to_owned()
}

#[cfg(feature="java")]
#[no_mangle]
pub extern "system" fn Java_Katla_loadFromConfig(
    mut env: JNIEnv,
    class: JClass,
    config: &JString,
) -> jobject {
    let config_string: String = env
        .get_string(config)
        .expect("Couldn't get java string!")
        .into();
    let output = env.new_object(class, "()V", &[]).unwrap();
    let katla = Katla::new_from_config(config_string, &Vec::new()).unwrap();
    //env.set_rust_field(output, "katlaReference", katla).unwrap();
    output.to_owned()
}

/*
#[no_mangle]
pub extern "system" fn Java_Katla_importFromConfig(mut env: JNIEnv, object: JObject, config: &JString) {
    let config_string: String = env
        .get_string(config)
        .expect("Couldn't get java string!")
        .into();
    let mut katla: MutexGuard<Katla> = env.get_rust_field(object, "katlaReference").unwrap();
    katla
        .import_config(config_string, &Vec::new())
        .unwrap();
}
 */

 #[cfg(feature="java")]
 #[no_mangle]
pub extern "system" fn Java_Katla_addCsvAppender(
    mut env: JNIEnv,
    object: JObject,
    name: &JString,
    file_name: &JString,
) {
    //let /*mut*/ katla :MutexGuard<Katla> = env.get_rust_field(object, "katlaReference").unwrap();
    println!("{:#?}", object);
    println!("{:#?}", String::from(env.get_string(name).unwrap()));
    println!("{:#?}", String::from(env.get_string(file_name).unwrap()));
}

/*
#[no_mangle]
pub extern "system" fn Java_Katla_generate(env: JNIEnv, object: JObject) {
    let mut katla: MutexGuard<Katla> = env.get_rust_field(object, "katlaReference").unwrap();
    katla.generate().unwrap();
}
 */
