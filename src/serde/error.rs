// --
// katla is a data flow generator
// --
// Copyright (C) 2019 Twimpiex <twimpiex@gnx.be>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License version 3 as
// published by the Free Software Foundation.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.
// --
// Be aware that at any time this program may introduce bugs, backward
// incompatibilities, undesired behaviour, backdoors, ...
// Trust your testing and quality assurance, don't trust on semantic
// versioning.
//
// Contact the copyright owner if you wish to use this program with
// another license.
// --

use std::error::Error;
use std::fmt::Debug;

#[derive(PartialEq, Debug, Clone)]
pub enum SerDeError {
    // Resource
    MissingCommaInUrl { url: String },
    UnknownUri { uri: String },
    UnknownUrlScheme { url: String, scheme: String },
}

impl std::fmt::Display for SerDeError {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        match self {
            SerDeError::MissingCommaInUrl { url } => {
                write!(f, "missing ',' in URL '{}'", url)
                //Err("no media type and no ','".to_string())
            }
            SerDeError::UnknownUri { uri } => {
                write!(f, "unknown URI '{}'", uri)
            }
            SerDeError::UnknownUrlScheme { url, scheme } => {
                write!(f, "unknown URL scheme '{}' in URL '{}'", scheme, url)
            }
        }
    }
}

impl Error for SerDeError {}
