// --
// katla is a data flow generator
// --
// Copyright (C) 2019 Twimpiex <twimpiex@gnx.be>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License version 3 as
// published by the Free Software Foundation.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.
// --
// Be aware that at any time this program may introduce bugs, backward
// incompatibilities, undesired behaviour, backdoors, ...
// Trust your testing and quality assurance, don't trust on semantic
// versioning.
//
// Contact the copyright owner if you wish to use this program with
// another license.
// --

use super::entity::Entity;

use indexmap::IndexMap;
use serde::{Deserialize, Serialize};

#[derive(Deserialize, Serialize, PartialEq, Eq, Debug, Clone)]
enum ContextName {
    Application,
    Entry,
    Local,
}

#[derive(Deserialize, Serialize, PartialEq, Debug, Clone)]
pub struct Context {
    name: ContextName,
    entities: IndexMap<String, Entity>,
}

impl Context {
    fn new(name: ContextName) -> Context {
        Context {
            name: name,
            entities: IndexMap::new(),
        }
    }

    // TODO get rid of this function and make sure that serialize of Context behaves the same as serialize of a map (entities), is needd for tera
    pub fn get_entities(&self) -> IndexMap<String, Entity> {
        self.entities.clone()
    }

    pub fn application() -> Context {
        Context::new(ContextName::Application)
    }

    pub fn entry() -> Context {
        Context::new(ContextName::Entry)
    }

    pub fn local() -> Context {
        Context::new(ContextName::Local)
    }

    pub fn insert(&mut self, key: Option<String>, value: Entity) -> Option<Entity> {
        match key {
            Some(key) => self.entities.insert(key, value),
            None => self
                .entities
                .insert(format!(" _-_ entity {} _-_ ", self.entities.len()), value),
        }
    }

    pub fn get(&self, key: &String) -> Option<&Entity> {
        self.entities.get(key)
    }

    pub fn get_key_value(&self, key: &String) -> Option<(&String, &Entity)> {
        self.entities.get_key_value(key)
    }

    pub fn get_index(&self, index: usize) -> Option<(&String, &Entity)> {
        self.entities.get_index(index)
    }

    pub fn iter(&self) -> indexmap::map::Iter<'_, String, Entity> {
        self.entities.iter()
    }

    pub fn to_json(&self) -> String {
        self.to_json_object(true, "".to_string(), "\t".to_string())
    }

    pub fn to_json_object(&self, last: bool, indentation: String, indent: String) -> String {
        let mut json = "".to_string();
        json.push_str("{\n");
        {
            let mut indentation = indentation.clone();
            indentation.push_str(&indent.clone());
            let mut length = self.entities.len();
            for entity in &self.entities {
                length -= 1;
                let last = length < 1;
                json.push_str(
                    entity
                        .1
                        .to_json_part(
                            Some(entity.0.to_string()),
                            last,
                            indentation.clone(),
                            indent.clone(),
                        )
                        .as_str(),
                );
            }
        }
        json.push_str(indentation.clone().as_str());
        json.push_str("}");
        if !last {
            json.push_str(",");
        }
        json.push_str("\n");
        json
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_context_new() {
        let context = Context::new(ContextName::Application);
        match context.name {
            ContextName::Application => {}
            _ => {
                assert!(false);
            }
        }
    }

    #[test]
    fn test_context_application() {
        let context = Context::application();
        match context.name {
            ContextName::Application => {}
            _ => {
                assert!(false);
            }
        }
    }

    #[test]
    fn test_context_serialize() {
        let mut context = Context::application();
        context.insert(Some("test 1".to_string()), Entity::I8(-8));
        context.insert(Some("test 2".to_string()), Entity::U8(8));
        context.insert(Some("test 3".to_string()), Entity::I64(-64));
        context.insert(Some("test 4".to_string()), Entity::U64(64));
        context.insert(Some("test 5".to_string()), Entity::I128(-128));
        context.insert(Some("test 6".to_string()), Entity::U128(128));
        context.insert(
            Some("test 7".to_string()),
            Entity::String("String".to_string()),
        );
        context.insert(None, Entity::String("None".to_string()));
        assert_eq!(
            serde_json::to_string(&context).unwrap(),
            "{\"name\":\"Application\",\"entities\":{\"test 1\":-8,\"test 2\":8,\"test 3\":-64,\"test 4\":64,\"test 5\":-128,\"test 6\":128,\"test 7\":\"String\",\" _-_ entity 7 _-_ \":\"None\"}}"
        );
    }
}
