// --
// katla is a data flow generator
// --
// Copyright (C) 2019 Twimpiex <twimpiex@gnx.be>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License version 3 as
// published by the Free Software Foundation.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.
// --
// Be aware that at any time this program may introduce bugs, backward
// incompatibilities, undesired behaviour, backdoors, ...
// Trust your testing and quality assurance, don't trust on semantic
// versioning.
//
// Contact the copyright owner if you wish to use this program with
// another license.
// --

use crate::eval::EvalError;

use super::context::Context;

use chrono::{DateTime, FixedOffset};
use indexmap::IndexMap;
use serde::{Deserialize, Serialize};
use std::fmt::Display;

#[derive(Deserialize, Serialize, PartialEq, Debug, Clone)]
#[serde(untagged)]
pub enum Entity {
    Bool(bool),
    I8(i8),
    U8(u8),
    I16(i16),
    U16(u16),
    I32(i32),
    U32(u32),
    I64(i64),
    U64(u64),
    I128(i128),
    U128(u128),
    Timestamp {
        // seconds since epoch in UTC
        seconds: i64,
        // nanoseconds since last second
        nanoseconds: Option<u32>,
        // time zone
        timezone: Option<i32>,
    },
    ISize(isize),
    USize(usize),
    F32(f32),
    F64(f64),
    Char(char),
    String(String),
    Array(Vec<Entity>),
    Map(IndexMap<String, Entity>),
    Context(Context),
}

impl Display for Entity {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(f, "{}", self.format(None, None).unwrap())
    }
}

impl Entity {
    pub fn format(
        &self,
        format: Option<&str>,
        format_args: Option<IndexMap<&str, Entity>>,
    ) -> Result<String, EvalError> {
        match self {
            Entity::Bool(value) => Ok(format!("{}", value)),
            Entity::I8(value) => Ok(format!("{}", value)),
            Entity::U8(value) => Ok(format!("{}", value)),
            Entity::I16(value) => Ok(format!("{}", value)),
            Entity::U16(value) => Ok(format!("{}", value)),
            Entity::I32(value) => Ok(format!("{}", value)),
            Entity::U32(value) => Ok(format!("{}", value)),
            Entity::I64(value) => Ok(format!("{}", value)),
            Entity::U64(value) => Ok(format!("{}", value)),
            Entity::I128(value) => Ok(format!("{}", value)),
            Entity::U128(value) => Ok(format!("{}", value)),
            Entity::Timestamp { .. } => self.format_timestamp(format, format_args),
            Entity::ISize(value) => Ok(format!("{}", value)),
            Entity::USize(value) => Ok(format!("{}", value)),
            Entity::F32(value) => Ok(format!("{}", value)),
            Entity::F64(value) => Ok(format!("{}", value)),
            Entity::Char(value) => Ok(format!("{}", value)),
            Entity::String(value) => Ok(format!("{}", value)),
            Entity::Array(..) => self.format_array(format, format_args),
            Entity::Map(..) => self.format_map(format, format_args),
            Entity::Context(..) => self.format_context(format, format_args),
        }
    }

    fn format_timestamp(
        &self,
        format: Option<&str>,
        _format_args: Option<IndexMap<&str, Entity>>,
    ) -> Result<String, EvalError> {
        if let Entity::Timestamp {
            seconds,
            nanoseconds,
            timezone,
        } = &self
        {
            let date_time =
                DateTime::from_timestamp(*seconds, nanoseconds.or_else(|| Some(0 as u32)).unwrap())
                    .expect("invalid timestamp")
                    .with_timezone(
                        &FixedOffset::east_opt(timezone.or_else(|| Some(0 as i32)).unwrap())
                            .unwrap(),
                    );
            let format = if let Some(format) = format {
                format
            } else {
                // Default formatting as if it was for fmt::Display
                match nanoseconds {
                    None => match timezone {
                        None => "%FT%T",
                        Some(_) => "%FT%T%::z",
                    },
                    Some(_) => match timezone {
                        None => "%FT%T%.9f",
                        Some(_) => "%FT%T%.9f%::z",
                    },
                }
            };
            Ok(format!("{}", date_time.format(format)))
        } else {
            Err(EvalError::NotImplemented)
        }
    }

    fn format_array(
        &self,
        format: Option<&str>,
        format_args: Option<IndexMap<&str, Entity>>,
    ) -> Result<String, EvalError> {
        if let Entity::Array(array) = &self {
            let mut parts = Vec::new();
            parts.push("{\n".to_string());
            for value in array {
                parts.push(format!(
                    "\t{}\n",
                    value.format(format, format_args.clone()).unwrap()
                ));
            }
            parts.push("}".to_string());
            Ok(parts.concat())
        } else {
            Err(EvalError::NotImplemented)
        }
    }

    fn format_map(
        &self,
        _format: Option<&str>,
        _format_args: Option<IndexMap<&str, Entity>>,
    ) -> Result<String, EvalError> {
        if let Entity::Map(map) = &self {
            let mut parts = Vec::new();
            parts.push("{\n".to_string());
            for (key, value) in map {
                parts.push(format!(
                    "\t\"{}\": {}\n",
                    key.to_string(),
                    value.to_string()
                ));
            }
            parts.push("}".to_string());
            Ok(parts.concat())
        } else {
            Err(EvalError::NotImplemented)
        }
    }

    fn format_context(
        &self,
        _format: Option<&str>,
        _format_args: Option<IndexMap<&str, Entity>>,
    ) -> Result<String, EvalError> {
        if let Entity::Context(context) = &self {
            Ok(format!("{{\n{:?}}}", context))
        } else {
            Err(EvalError::NotImplemented)
        }
    }

    pub fn to_json(&self) -> String {
        self.to_json_part(None, true, "".to_string(), "\t".to_string())
    }

    pub fn to_json_part(
        &self,
        attribute: Option<String>,
        last: bool,
        indentation: String,
        indent: String,
    ) -> String {
        let mut json = indentation.clone().to_string();
        match attribute {
            Some(attribute) => {
                json.push_str("\"");
                json.push_str(attribute.as_str());
                json.push_str("\": ");
            }
            None => {}
        }

        match &self {
            Entity::Bool(value) => {
                if *value {
                    json.push_str("true");
                } else {
                    json.push_str("false");
                }
            }
            Entity::I8(value) => {
                json.push_str(format!("{}", value).as_str());
            }
            Entity::U8(value) => {
                json.push_str(format!("{}", value).as_str());
            }
            Entity::I16(value) => {
                json.push_str(format!("{}", value).as_str());
            }
            Entity::U16(value) => {
                json.push_str(format!("{}", value).as_str());
            }
            Entity::I32(value) => {
                json.push_str(format!("{}", value).as_str());
            }
            Entity::U32(value) => {
                json.push_str(format!("{}", value).as_str());
            }
            Entity::I64(value) => {
                json.push_str(format!("{}", value).as_str());
            }
            Entity::U64(value) => {
                json.push_str(format!("{}", value).as_str());
            }
            Entity::I128(value) => {
                json.push_str(format!("{}", value).as_str());
            }
            Entity::U128(value) => {
                json.push_str(format!("{}", value).as_str());
            }
            Entity::Timestamp {
                seconds,
                nanoseconds,
                timezone,
            } => {
                json.push_str(
                    format!(
                        "{}",
                        DateTime::from_timestamp(
                            *seconds,
                            nanoseconds.or_else(|| { Some(0 as u32) }).unwrap()
                        )
                        .expect("invalid timestamp") //                    .with_timezone(TimeZone::from_offset(timezone.or_else(|| { Some(0 as i32) }).unwrap()))
                    )
                    .as_str(),
                );
            }
            Entity::ISize(value) => {
                json.push_str(format!("{}", value).as_str());
            }
            Entity::USize(value) => {
                json.push_str(format!("{}", value).as_str());
            }
            Entity::F32(value) => {
                json.push_str(format!("{}", value).as_str());
            }
            Entity::F64(value) => {
                json.push_str(format!("{}", value).as_str());
            }
            Entity::Char(value) => {
                json.push_str("\"");
                json.push(*value);
                json.push_str("\": ");
            }
            Entity::String(value) => {
                json.push_str("\"");
                json.push_str(value.as_str());
                json.push_str("\"");
            }
            Entity::Array(values) => {
                json.push_str("[");
                {
                    let mut indentation = indentation.clone();
                    indentation.push_str(&indent);
                    let mut length = values.len();
                    for value in values {
                        length -= 1;
                        let last = length < 1;
                        json.push_str(
                            value
                                .to_json_part(None, last, "".to_string(), indent.clone())
                                .as_str(),
                        );
                    }
                }
                json.push_str(indentation.clone().as_str());
                json.push_str("]");
            }
            Entity::Map(values) => {
                json.push_str("{\n");
                {
                    let mut indentation = indentation.clone();
                    indentation.push_str(&indent);
                    let mut length = values.len();
                    for value in values {
                        length -= 1;
                        let last = length < 1;
                        json.push_str(
                            value
                                .1
                                .to_json_part(
                                    Some(value.0.to_string()),
                                    last,
                                    indentation.clone(),
                                    indent.clone(),
                                )
                                .as_str(),
                        );
                    }
                }
                json.push_str(indentation.clone().as_str());
                json.push_str("}");
                if !last {
                    json.push_str(",");
                }
                json.push_str("\n");
            }
            Entity::Context(value) => {
                let mut indentation = indentation;
                indentation.push_str(&indent);
                json.push_str(value.to_json_object(last, indentation, indent).as_str());
            }
        }

        if !last {
            json.push_str(",");
        }
        json.push_str("\n");
        json
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use std::collections::HashMap;

    #[test]
    fn test_entity_timestamp_format() {
        assert_eq!(
            Entity::Timestamp {
                seconds: 0,
                nanoseconds: None,
                timezone: None
            }
            .format(Some("%Y-%m-%dT%H:%M:%S"), None)
            .unwrap(),
            "1970-01-01T00:00:00"
        );

        assert_eq!(
            Entity::Timestamp {
                seconds: 0,
                nanoseconds: None,
                timezone: None
            }
            .format(Some("%FT%T%.9f%::z"), None)
            .unwrap(),
            "1970-01-01T00:00:00.000000000+00:00:00"
        );

        assert_eq!(
            Entity::Timestamp {
                seconds: -132255941858,
                nanoseconds: None,
                timezone: Some(0),
                    }
            .format(Some("%FT%T%.9f%::z"), None)
            .unwrap(),
            "-2222-12-22T22:22:22.000000000+00:00:00"
        );
}

    #[test]
    fn test_entity_fmt_display() {
        assert_eq!(Entity::Bool(true).to_string(), "true");
        assert_eq!(Entity::Bool(false).to_string(), "false");
        assert_eq!(Entity::I8(-8).to_string(), "-8");
        assert_eq!(Entity::U8(8).to_string(), "8");
        assert_eq!(Entity::I16(-16).to_string(), "-16");
        assert_eq!(Entity::U16(16).to_string(), "16");
        assert_eq!(Entity::I32(-32).to_string(), "-32");
        assert_eq!(Entity::U32(32).to_string(), "32");
        assert_eq!(Entity::I64(-64).to_string(), "-64");
        assert_eq!(Entity::U64(64).to_string(), "64");
        assert_eq!(Entity::I128(-128).to_string(), "-128");
        assert_eq!(Entity::U128(128).to_string(), "128");

        assert_eq!(
            Entity::Timestamp {
                seconds: 0,
                nanoseconds: None,
                timezone: None
            }
            .to_string(),
            "1970-01-01T00:00:00"
        );
        assert_eq!(
            Entity::Timestamp {
                seconds: 0,
                nanoseconds: Some(0),
                timezone: None
            }
            .to_string(),
            "1970-01-01T00:00:00.000000000"
        );
        assert_eq!(
            Entity::Timestamp {
                seconds: 0,
                nanoseconds: None,
                timezone: Some(0)
            }
            .to_string(),
            "1970-01-01T00:00:00+00:00:00"
        );
        assert_eq!(
            Entity::Timestamp {
                seconds: 0,
                nanoseconds: Some(0),
                timezone: Some(0)
            }
            .to_string(),
            "1970-01-01T00:00:00.000000000+00:00:00"
        );
        assert_eq!(
            Entity::Timestamp {
                seconds: 1,
                nanoseconds: None,
                timezone: None
            }
            .to_string(),
            "1970-01-01T00:00:01"
        );
        assert_eq!(
            Entity::Timestamp {
                seconds: 1,
                nanoseconds: Some(1),
                timezone: None
            }
            .to_string(),
            "1970-01-01T00:00:01.000000001"
        );
        assert_eq!(
            Entity::Timestamp {
                seconds: 1,
                nanoseconds: None,
                timezone: Some(1)
            }
            .to_string(),
            "1970-01-01T00:00:02+00:00:01"
        );
        assert_eq!(
            Entity::Timestamp {
                seconds: 1,
                nanoseconds: Some(1),
                timezone: Some(1)
            }
            .to_string(),
            "1970-01-01T00:00:02.000000001+00:00:01"
        );
        assert_eq!(
            Entity::Timestamp {
                seconds: -1,
                nanoseconds: None,
                timezone: None
            }
            .to_string(),
            "1969-12-31T23:59:59"
        );
        assert_eq!(
            Entity::Timestamp {
                seconds: -1,
                nanoseconds: Some(1),
                timezone: None
            }
            .to_string(),
            "1969-12-31T23:59:59.000000001"
        );
        assert_eq!(
            Entity::Timestamp {
                seconds: -1,
                nanoseconds: None,
                timezone: Some(-1)
            }
            .to_string(),
            "1969-12-31T23:59:58-00:00:01"
        );
        assert_eq!(
            Entity::Timestamp {
                seconds: -1,
                nanoseconds: Some(1),
                timezone: Some(-1)
            }
            .to_string(),
            "1969-12-31T23:59:58.000000001-00:00:01"
        );

        assert_eq!(Entity::ISize(-1024).to_string(), "-1024");
        assert_eq!(Entity::USize(1024).to_string(), "1024");
        assert_eq!(Entity::F32(-32.23).to_string(), "-32.23");
        assert_eq!(Entity::F64(-64.46).to_string(), "-64.46");
        assert_eq!(Entity::Char('c').to_string(), "c");
        assert_eq!(Entity::String("String".to_string()).to_string(), "String");

        let mut array = Vec::new();
        array.push(Entity::I8(-8));
        array.push(Entity::U8(8));
        array.push(Entity::I64(-64));
        array.push(Entity::U64(64));
        array.push(Entity::I128(-128));
        array.push(Entity::U128(128));
        array.push(Entity::String("String".to_string()));
        assert_eq!(
            Entity::Array(array).to_string(),
            "{\n\t-8\n\t8\n\t-64\n\t64\n\t-128\n\t128\n\tString\n}"
        );

        let mut map = IndexMap::new();
        map.insert("test 1".to_string(), Entity::I8(-8));
        map.insert("test 2".to_string(), Entity::U8(8));
        map.insert("test 3".to_string(), Entity::I64(-64));
        map.insert("test 4".to_string(), Entity::U64(64));
        map.insert("test 5".to_string(), Entity::I128(-128));
        map.insert("test 6".to_string(), Entity::U128(128));
        map.insert("test 7".to_string(), Entity::String("String".to_string()));
        assert_eq!(Entity::Map(map).to_string(), "{\n\t\"test 1\": -8\n\t\"test 2\": 8\n\t\"test 3\": -64\n\t\"test 4\": 64\n\t\"test 5\": -128\n\t\"test 6\": 128\n\t\"test 7\": String\n}");

        // TODO Context(Context)
    }

    #[test]
    fn test_entity_serialize() {
        assert_eq!(serde_json::to_string(&Entity::I8(-8)).unwrap(), "-8");
        assert_eq!(serde_json::to_string(&Entity::U8(8)).unwrap(), "8");
        assert_eq!(serde_json::to_string(&Entity::I64(-64)).unwrap(), "-64");
        assert_eq!(serde_json::to_string(&Entity::U64(64)).unwrap(), "64");
        assert_eq!(serde_json::to_string(&Entity::I128(-128)).unwrap(), "-128");
        assert_eq!(serde_json::to_string(&Entity::U128(128)).unwrap(), "128");
        assert_eq!(
            serde_json::to_string(&Entity::String("String".to_string())).unwrap(),
            "\"String\""
        );

        let mut array = Vec::new();
        array.push(Entity::I8(-8));
        array.push(Entity::U8(8));
        array.push(Entity::I64(-64));
        array.push(Entity::U64(64));
        array.push(Entity::I128(-128));
        array.push(Entity::U128(128));
        array.push(Entity::String("String".to_string()));
        assert_eq!(
            serde_json::to_string(&Entity::Array(array)).unwrap(),
            "[-8,8,-64,64,-128,128,\"String\"]"
        );

        let mut map = HashMap::new();
        map.insert("test 1".to_string(), Entity::I8(-8));
        map.insert("test 2".to_string(), Entity::U8(8));
        map.insert("test 3".to_string(), Entity::I64(-64));
        map.insert("test 4".to_string(), Entity::U64(64));
        map.insert("test 5".to_string(), Entity::I128(-128));
        map.insert("test 6".to_string(), Entity::U128(128));
        map.insert("test 7".to_string(), Entity::String("String".to_string()));
        //assert_eq!(serde_json::to_string(&Entity::Map(map)).unwrap(), "String");
    }
}
