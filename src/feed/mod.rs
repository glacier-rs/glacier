// --
// katla is a data flow generator
// --
// Copyright (C) 2019 Twimpiex <twimpiex@gnx.be>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License version 3 as
// published by the Free Software Foundation.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.
// --
// Be aware that at any time this program may introduce bugs, backward
// incompatibilities, undesired behaviour, backdoors, ...
// Trust your testing and quality assurance, don't trust on semantic
// versioning.
//
// Contact the copyright owner if you wish to use this program with
// another license.
// --

use crate::{appender::AppenderContent, config::feed::Feed, model::context::Context};

use self::{csv::Csv, file::File, json::Json};

pub mod csv;
pub mod file;
pub mod json;

trait FeedType {
    fn to_appender_content(
        &mut self,
        context: &Context,
    ) -> std::result::Result<AppenderContent, Box<dyn std::error::Error>>;
}

fn into(feed: Feed) -> Box<dyn FeedType> {
    match feed {
        Feed::Csv { .. } => Box::new(Csv { feed: feed }),
        Feed::File { .. } => Box::new(File { feed: feed }),
        Feed::Json { content, .. } => Box::new(Json { content: content }),
    }
}

impl Feed {
    pub fn to_appender_content(
        &mut self,
        context: &Context,
    ) -> std::result::Result<AppenderContent, Box<dyn std::error::Error>> {
        let mut appender_type = into(self.clone());
        appender_type.to_appender_content(context)
    }
}
